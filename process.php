<!-- Created By: Marc Javier -->
<!-- Edited By:  -->
<?php
include ('order_class.php');

$fname = $minitial = $lname = $dob = $address = $city = $province = $postalcode = $country = $homephone = $busphone = $email = $cardnumber = $cardexpiry = $cardsecurity = $cardname = "";
$fnameErr = $minitialErr = $lnameErr = $dobErr = $addressErr = $cityErr = $provinceErr = $postalcodeErr = $countryErr = $homephoneErr = $busphoneErr = $emailErr = $cardnumberErr = $cardexpiryErr = $cardsecurityErr = $cardnameErr = "";

//Back end form validation incase javascript has been manipulated on the client side
//Can still bypass for prototype demonstration
if (isset($_POST['submit'])) {
    if (empty($_POST["first-name"])) {
        $fnameErr = "First name is required";
    } else {
        $fname = ($_POST["first-name"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $fname)) {
            $fnameErr = "Only letters and white space allowed";
        }
    }
    
    if (empty($_POST["mid-initial"])) {
        //Optional, returns NULL if value is empty
        $minitial = "";
    } else {
        $minitial = ($_POST["mid-initial"]);
    }
    
    if (empty($_POST["last-name"])) {
        $lnameErr = "Last name is required";
    } else {
        $lname = ($_POST["last-name"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $lname)) {
            $lnameErr = "Only letters and white space allowed";
        }
    }
        //TODO - validate to make it only accept 18 years and over
    if (empty($_POST["date-of-birth"])) {
        $dob = "Date of birth is required";
    } else {
        $dob = ($_POST["date-of-birth"]);
        // check if name only contains letters and whitespace
        // if (!preg_match("/^[a-zA-Z ]*$/", $dob)) {
        //     $dobErr = "Only letters and white space allowed";
        // }
    }

    if (empty($_POST["street-address"])) {
        $addressErr = "Address is required";
    } else {
        $address = ($_POST["street-address"]);
        // Regex allows for letters, numbers, hyphens, spaces, period and comma
        if (!preg_match("/^[A-Za-z0-9\-\\,.]+$/", $address)) {
            $addressErr = "Enter a valid street address";
        }
    }

    if (empty($_POST["city-address"])) {
        $cityErr = "City is required";
    } else {
        $city = ($_POST["city-address"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $city)) {
            $cityErr = "Only letters and white space allowed";
        }
    }
    //TODO -- check $_POST value because it's a selectable option, not input
    if (empty($_POST["province-address"])) {
        $provinceErr = "Province is Required";
    } else {
        $province = ($_POST["province-address"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $province)) {
            $provinceErr = "Only letters and white space allowed";
        }
    }

    if (empty($_POST["postal-code-address"])) {
        $postalcodeErr = "Province is Required";
    } else {
        $postalcode = ($_POST["postal-code-address"]);
        // check if value is a valid postal code format
        if (!preg_match("/[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]\d/", $postalcode)) {
            $postalcodeErr = "Enter a valid postal code format";
        }
    }
    //TODO - Canada is the only valid country for this prototype
    if (empty($_POST["country-address"])) {
        $countryErr = "Country is Required";
    } else {
        $country = ($_POST["country-address"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $country)) {
            $countryErr = "Only letters and white space allowed";
        }
    }

    if (empty($_POST["contact-home-phone"])) {
        $homephoneErr = "Phone number is required";
    } else {
        $homephone = ($_POST["contact-home-phone"]);
        // Checks for valid phone number combinations. Includes () - and spaces
        if (!preg_match("/\A[(]?[0-9]{3}[)]?[ ,-]?[0-9]{3}[ ,-]?[0-9]{4}\z/", $homephone)) {
            $homephoneErr = "Enter a valid phone number format";
        }
    }

    if (empty($_POST["contact-bus-phone"])) {
        $busphoneErr = "Phone number is required";
    } else {
        $busphone = ($_POST["contact-bus-phone"]);
        // Checks for valid phone number combinations. Includes brackets, dashes, commas and spaces
        if (!preg_match("/\A[(]?[0-9]{3}[)]?[ ,-]?[0-9]{3}[ ,-]?[0-9]{4}\z/", $busphone)) {
            $busphoneErr = "Enter a valid phone number format";
        }
    }
    
    if (empty($_POST["contact-email"])) {
        $emailErr = "Email address is required";
    } else {
        $email = ($_POST["contact-email"]);
        // check if name only contains letters and whitespace
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        }
    }

    if (empty($_POST["order-card-number"])) {
        $cardnumberErr = "Credit card number is required";
    } else {
        $cardnumber = ($_POST["order-card-number"]);
        // Regex for valid credit card number captures Visa, MasterCard, Discover, American Express, Diners Club, JCB
        if (!preg_match("/^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/", $cardnumber)) {
            $cardnumberErr = "Invalid card number";
        }
    }

    if (empty($_POST["order-card-expiry"])) {
        $cardexpiryErr = "Expiry date is required";
    } else {
        $cardexpiry = ($_POST["order-card-number"]);
        //TODO - change database to MM/YYYY
        // if (!preg_match()) {
        //     $cardexpiryErr = "Invalid expiry format";
        // }
    }

    if (empty($_POST["order-card-security"])) {
        $cardsecurityErr = "CVV is required";
    } else {
        $cardsecurity = ($_POST["order-card-security"]);
        //Regex to accept 3 or 4 digit security number
        if (!preg_match("/^[0-9]{3,4}$/")) {
            $cardexpiryErr = "Invalid expiry format";
        }
    }

    if (empty($_POST["order-card-name"])) {
        $cardnameErr = "Credit Card Holder Name is required";
    } else {
        $cardname = ($_POST["order-card-name"]);
        // check if value only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $cardname)) {
            $cardnameErr = "Only letters and white space allowed";
        }
    }
}

//Did not use $_POST as parameter as it will bypass backend validation
//Can also import Graeme's insert customer class?
// $order = new Order($fname, $minitial, $lname, $dob, $address, $city, $province, $postalcode, $country, $homephone, $busphone, $email, $cardnumber, $cardexpiry, $cardsecurity, $cardname);
$order = new Order($_POST);
processOrder($order);


// Process order to database
function processOrder() {

//Credentials
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "travelexperts";

//Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);

//Check connection
    if (!$conn) {
        trigger_error(mysqli_connect_error());
    } else {
        echo("Connection ok!<br />"); //TODO - take out for prototype launch
    }


    $bookingsKey = "(BookingDate, BookingNo, TravelerCount, CustomerId, TripTypeId)";
    $customersKey = "(CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustCountry, CustHomePhone, CustBusPhone, CustEmail)";
    $creditCardsKey = "(CCName, CCNumber, CCExpiry, CCSecurity)";

    $bookingsValue = sprintf("('%s', '%s', '%s', '%s', '%s')", BOOKING); //TODO - booking class
    //TODO

    $customersValue = sprintf("('%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s')", 
    $order -> getCustFirstName(), $order -> getCustMidInitial(), $order -> getCustLastName(), $order -> getCustDateOfBirth(), $order -> getCustAddress(), $order -> getCustCity(), $order -> getCustProvince(), $order -> getCustPostalCode(), $order -> getCustCountry(), $order -> getCustHomePhone(), $order -> getCustBusPhone(), $order -> getCustEmail());
    
    $creditCardValue = sprintf("('%s', '%s', '%s', '%s')", 
    $order -> getCustCardNumber(), $order -> getCustCardExpiry(), $order -> getCustCardSecurity(), $order -> getCustCardName());



    $bookingQuery = "INSERT into bookings $bookingsKey values $bookingsValue";
    $customersQuery = "INSERT into customers $customersKey values $customersValue";
    $creditCardQuery = "INSERT into creditcards $creditCardsKey values $creditCardValue";
    $bookingResult = mysqli_query($conn, $bookingQuery) or die("Booking Query Error");
    $customersResult = mysqli_query($conn, $customersQuery) or die("Customers Query Error");
    $creditCardResult = mysqli_query($conn, $creditCardQuery) or die("Credit Card Query Error");

    if ($bookingResult && $customersResult && $creditCardResult) {
        header("Location: order_summary.php"); //directs customer order summary page if process is successful
    } else {
        //TODO - return customer to form with error results
    }

//Close connection
    mysqli_close($conn);
}