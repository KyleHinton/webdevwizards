<!-- Author: Graeme Hodgkinson -->
<?php
    class Agent
    {
        //Fields match table columns in database
        private $AgtFirstName;
        private $AgtMiddleInitial;
        private $AgtLastName;
        private $AgtBusPhone;
        private $AgtEmail;
        private $AgtPosition;
        private $AgencyId;
        
        public function getAgtFirstName ()
		{
			return $this->AgtFirstName;
		}
			
		public function setAgtFirstName ($AgtFirstName)
		{
			$this->AgtFirstName = $AgtFirstName;
		}	

        public function getAgtMiddleInitial ()
		{
			return $this->AgtMiddleInitial;
		}
			
		public function setAgtMiddleInitial ($AgtMiddleInitial)
		{
			$this->AgtMiddleInitial = $AgtMiddleInitial;
		}		
				
		public function getAgtLastName ()
		{
			return $this->AgtLastName;
		}
			
		public function setAgtLastName ($AgtLastName)
		{
			$this->AgtLastName = $AgtLastName;
		}
			
		public function getAgtBusPhone ()
		{
			return $this->AgtBusPhone;
		}
			
		public function setAgtBusPhone ($AgtBusPhone)
		{
			$this->AgtBusPhone = $AgtBusPhone;
		}	
        
        public function getAgtEmail ()
		{
			return $this->AgtEmail;
		}
			
		public function setAgtEmail ($AgtEmail)
		{
			$this->AgtEmail = $AgtEmail;
		}	
        
        public function getAgtPosition ()
		{
			return $this->AgtPosition;
		}
			
		public function setAgtPosition ($AgtPosition)
		{
			$this->AgtPosition = $AgtPosition;
		}	
        
        public function getAgencyId ()
		{
			return $this->AgencyId;
		}
			
		public function setAgencyId ($AgencyId)
		{
			$this->AgencyId = $AgencyId;
		}	
        
        //Returns class fields as string formatted for sql insert: COLUMNS
        public function sqlColNames()
		{
			$property_array = (get_object_vars($this));	
            $property_string = implode(", ", array_keys($property_array));    
            $property_sql = "(" . $property_string . ")";
            return $property_sql;
        }
        
        //Returns object properties as string, formatted for sql insert: VALUES
        public function __toString()
		{
			return "'" . $this->AgtFirstName . "', '" . $this->AgtMiddleInitial . "', '" . $this->AgtLastName . "', '" . $this->AgtBusPhone . "', '" . $this->AgtEmail . "', '" . $this->AgtPosition . "', '" . $this->AgencyId . "'";
		}
        
         //Receives $_POST array as argument
		public function __construct($array)
		{
            //DB connection required to use my_sqli_real_escape_string, to protect from SQL injection
			$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
			if ($conn->connect_error) 
			{
				die("connection failed: " . $conn->connect_error);
			}
				
			$keys = array_keys($array);            
            if(array_key_exists('AgtFirstName', $array))
            {
                $this->setAgtFirstName(mysqli_real_escape_string($conn, $array['AgtFirstName']));
            }
            if(array_key_exists('AgtMiddleInitial', $array))
            {
                $this->setAgtMiddleInitial(mysqli_real_escape_string($conn, $array['AgtMiddleInitial']));
            }
            if(array_key_exists('AgtLastName', $array))
            {
                $this->setAgtLastName(mysqli_real_escape_string($conn, $array['AgtLastName']));
            }
            if(array_key_exists('AgtBusPhone', $array))
            {
                $this->setAgtBusPhone(mysqli_real_escape_string($conn, $array['AgtBusPhone']));	
            }
            if(array_key_exists('AgtEmail', $array))
            {
                $this->setAgtEmail(mysqli_real_escape_string($conn, $array['AgtEmail']));	
            }
			if(array_key_exists('AgtPosition', $array))
            {
                $this->setAgtPosition(mysqli_real_escape_string($conn, $array['AgtPosition']));
            }  
            if(array_key_exists('AgencyId', $array))
            {
                $this->setAgencyId(mysqli_real_escape_string($conn, $array['AgencyId']));
            }  
			$conn->close();	
		}
    }
?>