-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2018 at 04:33 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travelexperts`
--

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `PackageId` int(11) NOT NULL,
  `PkgName` varchar(50) NOT NULL,
  `PkgStartDate` date DEFAULT NULL,
  `PkgEndDate` date DEFAULT NULL,
  `PkgDesc` varchar(60) DEFAULT NULL,
  `PkgBasePrice` decimal(19,2) NOT NULL,
  `PkgAgencyCommission` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`PackageId`, `PkgName`, `PkgStartDate`, `PkgEndDate`, `PkgDesc`, `PkgBasePrice`, `PkgAgencyCommission`) VALUES
(1, 'Caribbean New Year', '2017-12-25', '2017-01-04', 'Cruise the Caribbean & Celebrate the New Year.', '4800.00', '400.00'),
(2, 'Polynesian Paradise', '2016-12-12', '2016-12-20', '8 Day All Inclusive Hawaiian Vacation', '3000.00', '310.00'),
(3, 'Asian Expedition', '2016-05-14', '2016-05-28', 'Airfaire, Hotel and Eco Tour.', '2800.00', '300.00'),
(4, 'European Vacation', '2016-11-01', '2016-11-14', 'Euro Tour with Rail Pass and Travel Insurance', '3000.00', '280.00'),
(5, 'Inca Trail', '2018-11-01', '2018-11-07', '7 Days of trekking adventure to Machu Picchu', '3000.00', '280.00'),
(6, 'Australian Outback', '2018-12-03', '2018-12-08', 'Enjoy some shrimp on the barbie from the land down under', '5000.00', '400.00'),
(7, 'Winter Escape Package', '2018-12-29', '2019-01-01', 'Escape the city and spend NYE in the mountains', '2000.00', '110.00'),
(8, 'Greece Lightning', '2018-08-13', '2018-08-18', 'Discover the most beautiful beaches of Greece', '4800.00', '300.00'),
(9, 'Wakanda Expedition', '2020-06-08', NULL, 'Harvest the rarest and strongest metal on Earth, vibranium', '125000.00', '10000.00'),
(10, 'Above and Beyond', '2021-04-22', '2021-04-23', 'Head past earth\'s stratosphere and enjoy views from space', '250000.00', '5000.00'),
(11, 'Total Recall', '2022-12-12', '2023-01-12', 'Travel Experts Exclusive! Ride a SpaceX rocket to Mars!', '1000000.00', '25000.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`PackageId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `PackageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;