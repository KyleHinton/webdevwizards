
<!-- Designed and Coded By:  Carol Bing He -->

        <?php

            require_once('mysqli_connect.php');
            define ("TITLE","Order Summary");
            include ("header.php");

            //receive booking id from booking submission and store it in the variable $bookingId
            $bookingId = $_GET['bookingId'];

            //to get customer id by booking id
            $sql = "SELECT CustomerId FROM bookings where bookings.BookingId = '$bookingId';";
            $result = mysqli_query($conn, $sql);
            if($result) {
                while ($row=$result->fetch_assoc()) {
                    $customerId = $row['CustomerId'];
                }
            }

        ?>

        <style>
            .table_name, #title {
                background:#dbdee0;
			    color:#5d6675;
                padding:5px;
                margin:0;
            }

            #printBtn {
                padding:3px 15px;
            }

            #div3 {
                background:#dbdee0;
            }

            #div3 p {
                text-align:center;
                margin:0;
                font-size:80%;
            } 
        </style>

        <main class="container">
            <br><br>
            
            <div class="row" id="div1">
                <!--display 'booking success' message to give a feedback to customer that booking is received-->
                <span class="col" style="color:#5d6675;font-size:110%;"><b>Congratulations ! Your booking is confirmed.</b></span>   
                <!--provide a print button for customer to conveniently print the receipt and attach an onclick event to print button-->
                <span><button class="col" id="printBtn" onclick="print();">Print</button></span><br><br>
            </div><br>

            <div id="div2"> 
                <!-- information in the order summary, i.e., the receipt, will be organized in four rows:-->
                <!--1. Customer Information   2.Booking Information   3.Package Information   4.Total Price-->

                <h3  id="title" align="center" style="margin-bottom:0;">Order Summary</h3><br>
               
                <!-- The following code is for Customer Information row in the receipt-->  
                <?php
                //extract customer data from customers table and display the data in a table
                $query_customer = "select customers.* from customers where customers.CustomerId = $customerId;";
               
                $result_customer = @mysqli_query($conn, $query_customer);

                if($result_customer) {$row = mysqli_fetch_array($result_customer);}
                ?>
                <p class="table_name"><b>Customer Information</b></p>
                <table class="table table-striped table-bordered table-hover table-responsive table-sm w-100 d-block d-md-table" style="font-size:80%;">
                    <thead>
                        <tr>
                            <th align="left">Customer Id</th>
                            <th align="left">First Name</th>
                            <th align="left">Last Name</th>
                            <th align="left">Address</td>
                            <th align="left">City</th>
                            <th align="left">Province</th>
                            <th align="left">Country</th>
                            <th align="left">Postal Code</th>
                            <th align="left">Home Phone</th>
                            <th align="left">Business Phone</th>
                            <th align="left">Email</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                 
                        <tr>
                            <td align="left"><?php echo $row['CustomerId']; ?></td>
                            <td align="left"><?php echo $row['CustFirstName']; ?></td>
                            <td align="left"><?php echo $row['CustLastName'];?></td>
                            <td align="left"><?php echo $row['CustAddress'] ?></td>
                            <td align="left"><?php echo $row['CustCity']; ?></td>
                            <td align="left"><?php echo $row['CustProv']; ?></td>
                            <td align="left"><?php echo $row['CustCountry']; ?></td>
                            <td align="left"><?php echo $row['CustPostal']; ?></td>
                            <td align="left"><?php echo $row['CustHomePhone']; ?></td>
                            <td align="left"><?php echo $row['CustBusPhone']; ?></td>
                            <td align="left"><?php echo $row['CustEmail']; ?></td>
                        </tr>
            
                   </tbody>
                </table><br>


            <!-- The following code is for Booking Information row in the receipt-->    

                   <?php
                        //extract booking data from bookings table and display the necessary data in a table
                        $sql_booking = "SELECT * FROM bookings where bookings.BookingId = '$bookingId';";
                        $result_booking = mysqli_query($conn, $sql_booking);
                        if($result_booking) {$row_booking = mysqli_fetch_array($result_booking);}
                    ?>       
                             
                
                <p class="table_name"><b>Booking Information</b></p>
                <table class="table table-striped table-bordered table-hover table-responsive table-sm" style="font-size:80%;">
                    <thead>
                        <tr>
                            <th align="left">Booking Id</th>
                            <th align="left">Booking Date</th>
                            <th align="left">Booking No.</b></th>
                            <th align="left">Traveler Count</td>
                            <th align="left">Customer Id</th>
                            <th align="left">Trip Type Id</th>
                            <th align="left">Package Id</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        <tr>
                            <td align="left"><?php echo $row_booking['BookingId']; ?></td>
                            <td align="left"><?php echo $row_booking['BookingDate']; ?></td>
                            <td align="left"><?php echo $row_booking['BookingNo']; ?></td>
                            <td align="left"><?php echo $row_booking['TravelerCount']; ?></td>
                            <td align="left"><?php echo $row_booking['CustomerId']; ?></td>
                            <td align="left"><?php echo $row_booking['TripTypeId']; ?></td>
                            <td align="left"><?php echo $row_booking['PackageId']; ?></td>
                        </tr>
            
                   </tbody>
                </table><br>

            <!-- The following code is for Package Information row in the receipt--> 

                <?php
                    //to get package id by booking id so that package data will be extracted from database by package id
                    $sql_pkg_id = "SELECT PackageId FROM bookings where bookings.bookingId = '$bookingId';";
                    $result_pkg_id = mysqli_query($conn, $sql_pkg_id);
                    if($result_pkg_id) {
                        while ($row_pkg_id=$result_pkg_id->fetch_assoc()) {
                            $packageId = $row_pkg_id['PackageId'];
                        }
                    }
                    //extract package data from packagess table and display the necessary data in a table
                    $query_itinerary = "select * from packages where packages.PackageId = '$packageId';";

                    $result_itinerary = @mysqli_query($conn, $query_itinerary);

                    if($result_itinerary) {$row_itinerary = mysqli_fetch_array($result_itinerary);}
                ?>

                <p class="table_name"><b>Package Information</b></p>
                <table class="table table-striped table-bordered table-hover table-responsive table-sm" style="font-size:80%;">
                <thead>
                    <tr>
                    
                        <th align="left">Pkg Name</th>
                        <th align="left">Pkg Start Date</b></th>
                        <th align="left">Pkg End Date</td>
                        <th align="left">Pkg Description</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        
                        <td align="left"><?php echo $row_itinerary['PkgName']; ?></td>
                        <td align="left"><?php echo $row_itinerary['PkgStartDate']; ?></td>
                        <td align="left"><?php echo $row_itinerary['PkgEndDate']; ?></td>
                        <td align="left"><?php echo $row_itinerary['PkgEndDate']; ?></td>
                    </tr>

                </tbody>
                </table><br>

                

        <!-- The following code is to calculate total price--> 
        <?php
            //to get base price needs 2 steps
            //base price step 1:to get package id by booking id so that package base price will be extracted from database by package id
            $sql_pkg_id = "SELECT PackageId FROM bookings where bookings.bookingId = '$bookingId';";
            $result_pkg_id = mysqli_query($conn, $sql_pkg_id);
            if($result_pkg_id) {
                while ($row_pkg_id=$result_pkg_id->fetch_assoc()) {
                    $packageId = $row_pkg_id['PackageId'];
                }
            }

            //base price step 2:get base price from packages table by package id
            $sql_base_price = "SELECT PkgBasePrice FROM packages where packages.PackageId = '$packageId';";
            $result_base_price = mysqli_query($conn, $sql_base_price);
            if($result_base_price) {
                while ($row_base_price=$result_base_price->fetch_assoc()) {
                    $basePrice = $row_base_price['PkgBasePrice'];
                }
            }

            //finding Booking Fee needs 2 stpes
            //booking fee step 1: get fee id from bookingdetails table by booking id
            $sql_fee_id = "SELECT feeId FROM bookingdetails where bookingdetails.BookingId = '$bookingId';";

            $result_fee_id = mysqli_query($conn, $sql_fee_id);
            if($result_fee_id) {
                while ($row_fee_id = $result_fee_id->fetch_assoc()) {
                    $feeId = $row_fee_id['feeId'];
                }
            }
            //booking fee step 2: get booking fee from fees table by fee id
            $sql_booking_fee = "SELECT feeAmt FROM fees where fees.FeeId = '$feeId';";
            $result_booking_fee = mysqli_query($conn, $sql_booking_fee);
            if($result_booking_fee) {
                while ($row_booking_fee=$result_booking_fee->fetch_assoc()) {
                    $bookingFee = $row_booking_fee['feeAmt'];
                }
            }
            //calculate tax and then total price
            $tax = ($basePrice + $bookingFee)*0.05;
            $total = $basePrice + $bookingFee + $tax;

        ?>
        <!--list base price, booking fee, tax and total price in a table-->
        <p class="table_name"><b>Total Price</b></p>
        <table class="table table-striped table-bordered table-hover table-responsive table-sm" style="font-size:80%;">
                    <thead>
                        <tr>
                            <th align="left">Base Price</th>
                            <th align="left">Booking Fee</th>
                            <th align="left">Tax</td>
                            <th align="left">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                
                        <tr>
                            <td align="left"><?php echo $basePrice; ?></td>
                            <td align="left"><?php echo $bookingFee; ?></td>
                            <td align="left"><?php echo $tax; ?></td>
                            <td align="left"><?php echo $total; ?></td>
                        </tr>

                </tbody>
                </table><br>
                <div id="div3">
                    <!--it is necessary to include travel experts' company address, phone number and fax number in the receipt-->
                    <p>----------------------------------------------------------Travel Experts-------------------------------------------------------------</p>
                    <p>Calgary office: 1155 8th Ave SW	Calgary	AB T2P1N3 Canada Phone:(403)271-9873 Fax:(403)271-9872</p>
                    <p>Okatoks office:	110 Main Street	Okotoks	AB T7R3J5 Canada Phone:(403)563-2381 Fax:(403)563-2382</p>
                </div>
                
            </div>
        </main><br><br>

        <script>

            //print() is for the print button
            function print() {
                var prtContent = document.getElementById("div2");
                var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
            }
        </script>

        <?php
        
        include ("footer.php");
        
        ?>
        
        <?php
        
        mysqli_close($conn);
        
        ?>

   











	