<!-- Found by: Graeme Hodgkinson -->
<?php
    //copy pasted from web https://stackoverflow.com/questions/4356289/php-random-string-generator
    function randomString($length = 6) 
    {
        return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
?>