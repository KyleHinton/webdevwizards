<!-- Created by Graeme Hodgkinson and Kyle Hinton -->
<?php 
include("function.php");
include("header.php");
?>
<html>
    <style>
        form {
            text-align:center;
        }
    </style>
    <body>
        <br>
        <!-- Register Modal -->      
            <div id="myModal" class="modal">   
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">            
                        <button type="button" class="close" data-dismiss="modal">&times;</button> 
                        <p style="text-align:center">Agent record created</p>
                    </div>
                </div>
            </div>
            
            <!--trigger modal if redirected from agent registration-->
            <?php
                if (isset($_GET['confirm']) && 1 == $_GET['confirm']) 
                {
                    echo "
                    <script>
                        var modal = document.getElementById('myModal');
                        modal.style.display = 'block';
                    </script>
                    ";
                }
            ?>  
        <form action="TravelAgentRegister.php">
            <input type="submit" value="Register New Agent" />
        </form>
        <br>
        <form action="TravelAgentCustomerView.php">
            <input type="submit" value="View Customer Database" />
        </form>	
    <br>
        <br>
        <form action="PackageCreation.php">
            <input type="submit" value="Create Package" />
        </form>	
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php
    include ("footer.php");
    ?>
    </body>
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');        
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() 
        {
            modal.style.display = "none";
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
            }
        }
    </script>
</html>