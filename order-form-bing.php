<!-- Created by: Marc Javier
      Edited by: Bing He -->

<!-- DELETE THIS FILE -->

<?php
		define ("TITLE","Order Package");
		include ("header.php");
?>
<style>
  main {
    background:#07537f;
    color:white;
    padding-left:25%;
    padding-right:25%;
  }
  .passenger-information,.contact-information,.payment-information,#last_div  {
    background:#07537f;
    color:white;
  }
  #last_div {
    background:#0a71ad;
  }
  
</style>

<main>
  <form action="#" method="post" autocomplete="off">
    <fieldset class="passenger-information">
      <legend>Passenger Information</legend>
        <p>Enter exactly as stated on your passport to avoid future problems.</p>
        <p>All fields are required</p>
       
            <div class="form-group">
              Title
              <select name="personal-title" class="form-control" id="personal-title">
                <option value="title-blank"></option>
                <option value="title-mister">Mr.</option>
                <option value="title-mrs">Mrs.</option>
                <option value="title-ms">Ms.</option>
              </select>
            </div>

            <div class="form-group">
              First Name
              <input type="text" name="first-name" class="form-control" id="first-name" placeholder="First Name" required></td>
            </div>

            <div class="form-group">
              Middle Name
              <input type="text" name="mid-initial"  class="form-control"id="mid-initial" placeholder="Middle Name" required></td>
            </div>

            <div class="form-group">
              Last Name
              <input type="text" name="last-name" class="form-control" id="last-name" placeholder="Last Name" required></td>
            </div>
            
            <div class="form-group">
              Date of Birth
              <input type="date" name="date-of-birth" class="form-control" id="date-of-birth">
            </div>
    </fieldset>
    
    <fieldset class="contact-information">
      <h3>Contact Information</h3>

      <div class="form-group">
        <label for="street_address">Address*</label>
        <input type="text" name="street-address" class="form-control" id="street_address" required>
      </div>

      <div class="form-group">
        <label for="city_address">City*</label>
        <input type="text" name="city-address" class="form-control" id="city_address">
      </div>

      <div class="form-group">
        <label for="province_address">Province*</label>
        <select name="province-address" class="form-control" id="province_address" required>
          <option disabled selected>Select Province</option>
          <option value="AB">Alberta</option>
          <option value="BC">British Columbia</option>
          <option value="MB">Manitoba</option>
          <option value="NB">New Brunswick</option>
          <option value="NL">Newfoundland and Labrador</option>
          <option value="NS">Nova Scotia</option>
          <option value="NT">Northwest Territories</option>
          <option value="NU">Nunavut</option>
          <option value="ON">Ontario</option>
          <option value="PE">Prince Edward Island</option>
          <option value="QC">Quebec</option>
          <option value="SK">Saskatchewan</option>
          <option value="YT">Yukon Territories</option>
        </select>
      </div>

      <div class="form-group">
        <label for="postal-code">Postal Code*</label>
        <input type="text" name="postal-code-address" class="form-control" id="postal_code_address" required>
      </div>

      <div class="form-group">
        <label for="country-address">Country*</label>
        <select name="country-address" class="form-control" id="country-address">
          <option selected>Select Country</option>
          <option value="CANADA" selected>Canada</option>
          <option value="USA" selected>USA</option>
        </select>
      </div>

    </fieldset>

    <fieldset class="payment-information">
      <legend>Payment Information</legend>
      <div class="form-group">
        <label for="cc-number">Card Number*</label>
        <input type="tel" class="form-control">
      </div>

      <div class="form-group">
      <label for="cc-expiry">Expiry Date*</label>
      <input type="text" class="form-control">
      </div>

      <div class="form-group">
      <label for="cvv">Security Number*</label>
      <input type="text" class="form-control">
      </div>

      <div class="form-group">
        <label for="cc-name">Cardholder Name*</label>
        <input type="text" class="form-control">
      </div>

    </fieldset>

    <div id="last_div">
      <div class="form-group">
        <input type="checkbox" name="booking-certify" id="">
        <label for="terms-and-conditions">I certify that the above information is correct...</label><span class="required">*</span>
      </div>

      <div class="terms-and-conditions">
        <input type="checkbox" name="booking-terms" id="terms-and-conditions">
        <label for="terms-and-conditions">I agree with Travel Experts terms &amp; conditions...</label><span class="required">*</span>
      </div>

      <div class="controls">
        <button type="submit" class="btn btn-success">Back</button>
        <button type="submit" class="btn btn-danger">Complete Booking</button>
      </div>
    </div>
  </form>
</main>

<?php
		include ("footer.php");
?>
