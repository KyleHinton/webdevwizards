<!-- insert customer record, booking record, booking detail record, and credit card record to database -->
<?php
    include "customer_class.php";
    include "cc_class.php";
    include "booking_class.php";
    include "booking_details_class.php";
    include "insert2.php";
    
    //insert customer record
    $table_name = "customers";
    if(isset($_REQUEST))
    {
        $customer = new Customer($_POST);
    }		
	$columns = $customer->sqlColNames();
	Insert2($customer, $table_name, $columns);   

    //create booking object    
    if(isset($_REQUEST))
    {
        $booking = new Booking($_POST);
    }
        
    //use username from form input to get the newly created customerId from db
    $conn = new mysqli("localhost", "root", "", "travelexperts"); 			
	if ($conn->connect_error) 
    {
		die("connection failed: " . $conn->connect_error);
	}
    $user = $_POST['username'];
    $user = mysqli_real_escape_string($conn, $user);
    $sql = "SELECT CustomerId FROM customers WHERE Username='$user'";    
	$result = $conn->query($sql);    
	$conn->close();
    //extract customer ID from sql result array
    while($row = mysqli_fetch_array($result))
    {
        $customerId = $row[0];
    }    
    $booking->setCustomerId($customerId);    
    //insert booking record
    $table_name = "bookings";
    $columns = $booking->sqlColNames();
    Insert2($booking, $table_name, $columns);
    
    //create credit card object    
    if(isset($_REQUEST))
    {
        $cc = new Credit_Card($_POST);
    }         
    $cc->setCustomerId($customerId);    
    //insert booking record
    $table_name = "creditcards";
    $columns = $cc->sqlColNames();
    Insert2($cc, $table_name, $columns);
    
    //create booking details object    
    if(isset($_REQUEST))
    {
        $details = new BookingDetails($_POST);
    }
        
    //get the new bookingID from db using the newly made customerId
    $conn = new mysqli("localhost", "root", "", "travelexperts"); 			
	if ($conn->connect_error) 
    {
		die("connection failed: " . $conn->connect_error);
	}
    $sql = "SELECT BookingId FROM bookings WHERE CustomerId=".  $customerId;		
	$result = $conn->query($sql);
	$conn->close();
    //extract booking ID from sql result array
    while($row = mysqli_fetch_array($result))
    {
        $bookingId = $row[0];
    }    
    $details->setBookingId($bookingId);
    //insert booking details record
    $table_name = "bookingdetails";
    $columns = $details->sqlColNames();
    Insert2($details, $table_name, $columns);
    //redirect to order_summary and pass the inserted booking id
    header('Location: order_summary.php?bookingId='.$bookingId);
?>