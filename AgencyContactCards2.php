




<?php
include("bootstrapper.php");
/*----------------------------------------------*/
/*Author: Kyle Hinton							*/
/*Inspiration: Marc Javier						*/
/*Student ID: 000776182							*/
/*E-mail: kyle.hinton@edu.sait.ca				*/
/*-----------------------------------------------*/
//This script will get the agency contact information and display it in a table.

//Variables for mysqli server

include("header.php");
$server = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";
$agentdb = "(AgentId, AgtFirstName, AgtMiddleInitial, AgtLastName, AgtBusPhone, AgtEmail, AgtPosition, AgencyId)";
$agencydb = "(AgencyId, AgncyAddress, AgncyCity, AgncyProv, AgncyPostal, AgncyCountry, AgncyPhone, AgncyFax)";
$image_url =

//Creating a connection objection
$mysqli = new mysqli($server, $username, $password, $dbname);
$agentsqli = new mysqli($server, $username, $password, $dbname);


//If there is a connection error disconnect
if($mysqli-> connect_error){
		die("Error number: " . $mysqli->connect_error); //exit if there is a connection error. print the error message.
	}
if($agentsqli-> connect_error){
		die("Error number: " . $agentsqli->connect_error); //exit if there is a connection error. print the error message.
	}	
	
	else {echo "Connection to Agencies and Agents Successful!";};
//get the query from the agencies table
$key = $agencydb;
$query = "SELECT * FROM agencies";
//check if query matches
$result = mysqli_query($mysqli, $query) or die("There was a query error");

$key = $agentdb;
$query2 = "SELECT * FROM agents";
//check if query matches
$result2 = mysqli_query($agentsqli, $query2) or die("There was a query error");


//if so, create a table
if($result && $result2){
$agtId = 0;
$offset = 0;
$cellCount = 0;

while($theline = $result->fetch_assoc()){
	$agtId++;
//	echo "top of loop $agtId";
//echo 
//'<table border "1">
//	<tr>
//		<td>AgencyId</td>
//		<td>Address</td>
//		<td>City</td>
//		<td>Province</td>
//		<td>Postal Code</td>
//		<td>Country</td>
//		<td>Phone</td>
//		<td>Fax</td>
//	</tr>';
	

echo 	"<div class =\"col-md-1\"/>";
echo "<img src='images/adventuremap.jpg' height=\"100\" width=\"100\" />";
echo "<div class = \"container\" float: \"left\">";
echo "<br><h3><b>Travel Experts</b><h3>";
echo "<br><h4><b> ". $theline["AgncyCity"] ."</b></h4>";
echo "<br><h5><label>" .  $theline["AgncyPhone"] . "</label></h5>";
echo "</div>";


//echo	'<td>' . $theline["AgencyId"] . '</td>';
//echo	'<td>' . $theline["AgncyAddress"] . '</td>';
//echo	'<td>' . $theline["AgncyCity"] . '</td>';
//echo	'<td>' . $theline["AgncyProv"] . '</td>';
//echo	'<td>' . $theline["AgncyPostal"] . '</td>';
//echo	'<td>' . $theline["AgncyCountry"] . '</td>';
//echo	'<td>' . $theline["AgncyPhone"] . '</td>';
//echo	'<td>' . $theline["AgncyFax"] . '</td>';
//echo	'</tr>';
//echo 
//'<table border "1">
//	<tr>
//		<td>Agent Id</td>
//		<td>Agent Name</td>
//		<td>Middle Initial</td>
//		<td>Last Name</td>
//		<td>Agent Phone Number</td>
//		<td>Agent E-mail</td>
//		<td>Agent Position</td>
//		<td>Agency Id</td>
//	</tr>';
	
	while($aline = $result2->fetch_assoc()){
//		if($cellCount >= 4){
//			echo "<div class=\"row\">";
//		}
		if($aline["AgencyId"] == $agtId){
			
			
echo "<div class = \"card\" border \"1\"/>";
echo "<img src='images/gotravel.jpg' height=\"100\" width=\"100\" />";
echo "<div class = \"container\ float: \"left\">";
echo "<h3><b>". $aline["AgtFirstName"] . $aline["AgtLastName"] ."</b><h3>";
//echo "<h4><b> ". $aline["AgtFirstName"] ."</b></h4>";
echo "<h5><label>" .  $aline["AgtBusPhone"] . "</label></h5>";
echo "<h5><label>" .  $aline["AgtEmail"] . "</label></h5>";
echo "<h6><label>" .  $aline["AgtPosition"] . "</label></h6>";
echo "</div>";
		
			echo 	'<tr>';
			echo	'<td>' . $aline["AgentId"] . '</td>';
			echo	'<td>' . $aline["AgtFirstName"] . '</td>';
			echo	'<td>' . $aline["AgtMiddleInitial"] . '</td>';
			echo	'<td>' . $aline["AgtLastName"] . '</td>';
			echo	'<td>' . $aline["AgtBusPhone"] . '</td>';
			echo	'<td>' . $aline["AgtEmail"] . '</td>';
			echo	'<td>' . $aline["AgtPosition"] . '</td>';
			echo	'<td>' . $aline["AgencyId"] . '</td>';
			echo	'</tr>';

//		if($cellCount >= 4){
//			echo "</div>";
//		$cellCount = 0;}
//	++$cellCount;
//			};

};


//reset($result2);

//echo $agtId . "Count";
//$result2 = mysqli_query($agentsqli, $query2) or die("There was a query error");

}; //end of double while loop
};
};//else{
//	echo "No Results";
//};

 include("footer.php");

?>


<!DOCTYPE html>
<html>
<body>
<a href = "UserLogin.Html"  style = "margin-right: 100%";>Login</a>
</body>
</html>

