<!-- Author: Graeme Hodgkinson -->
<!-- Validation Integration By: Marc Javier -->
<?php
    define ("TITLE","Customer Registration");
    include ("header.php");    
?>
<html>
    <style>

    main {
        background:white;
        color:black;
        padding:10px;
    }

    form {
        text-align:left;        
    }

    h3 {
        margin: 0 auto;
    }

    #buttons {    
        center
    }

    </style>

    <main>    
        <h3 style="text-align: center"> Customer Registration </h3><br>
        <form class="needs-validation" id="myForm" method="post" action="insert_cust2.php" novalidate>
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">First Name</span>
                        </div>
                        <input type="text" name="first" class="form-control" id="p1" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="First Name" required>
                        <div class="invalid-feedback">Please provide a valid name.</div>
                    </div>  
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Last Name</span>
                        </div>
                        <input type="text" name="last" class="form-control" id="p2" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Last Name" required>
                        <div class="invalid-feedback">Please provide a valid name.</div>
                    </div>  
                </div>
            </div>
                
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Address</span>
                            </div>
                            <input type="text" name="address" class="form-control" id="p3" maxlength="75" placeholder="Address" required>
                            <div class="invalid-feedback">Please provide a valid address.</div>
                        </div>  
                    </div>
                </div>   
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">City</span>
                            </div>
                            <input type="text" name="last" class="form-control" id="p2" maxlength="50" pattern="^[A-Za-z -]+$" placeholder="City" required>
                            <div class="invalid-feedback">Please provide a valid city.</div>
                        </div>  
                    </div>
                </div>     
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Province</span>
                            </div>
                            <select class="custom-select" name="province" id="p5" required>
                            <option disabled selected>Select Province</option>
                            <option value="AB">Alberta</option>
                            <option value="BC">British Columbia</option>
                            <option value="MB">Manitoba</option>
                            <option value="NB">New Brunswick</option>
                            <option value="NL">Newfoundland and Labrador</option>
                            <option value="NS">Nova Scotia</option>
                            <option value="NT">Northwest Territories</option>
                            <option value="NU">Nunavut</option>
                            <option value="ON">Ontario</option>
                            <option value="PE">Prince Edward Island</option>
                            <option value="QC">Quebec</option>
                            <option value="SK">Saskatchewan</option>
                            <option value="YT">Yukon Territories</option>
                        </select>
                            <div class="invalid-feedback">Please provide a valid province.</div>
                        </div>  
                    </div>
                </div>   

                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Postal Code*</span>
                            </div>
                            <input type="text" name="postal" class="form-control" id="p6" maxlength="7" pattern="[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz] ?[0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]" placeholder="ex. T2E 9B5 or T2E9B5" required>
                            <div class="invalid-feedback">Please provide a valid postal code.</div>
                        </div>  
                    </div>
                </div>  

                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Country</span>
                            </div>
                            <select class="custom-select" name="country" id="p7" required>
                                <option selected>Canada</option>
                            </select>                        
                        </div>  
                    </div>
                </div>            

                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Home Phone</span>
                            </div>
                            <input type="text" name="home#" class="form-control" id="p8" pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$" placeholder="Home Phone" required>
                            <div class="invalid-feedback">Please provide a valid phone number.</div>
                        </div>  
                    </div>
                </div>              
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Bus. Phone</span>
                            </div>
                            <input type="text" name="bus#" class="form-control" id="p9" pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$" placeholder="Business Phone">
                            <div class="invalid-feedback">Please provide a valid phone number.</div>
                        </div>  
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Email Address</span>
                            </div>
                            <input type="email" name="email" class="form-control" id="p10" placeholder="example@demo.com" required>
                            <div class="invalid-feedback">Please provide a valid email.</div>
                        </div>  
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Username</span>
                            </div>
                            <input type="text" name="username" class="form-control" id="p8" pattern="[a-zA-Z0-9-]+" onblur="checkUsername(this.value)" placeholder="Username" required>
                            <div class="invalid-feedback">Please provide a valid username. Numbers and letters only.</div>
                            <div id="Msg"></div>
                        </div>  
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password</span>
                            </div>
                            <input type="password" name="password" class="form-control" id="p8" pattern="[a-zA-Z0-9-]+" placeholder="Password" required>
                            <div class="invalid-feedback">Please provide a valid password. Numbers and letters only.</div>
                        </div>  
                    </div>
                </div>   
                <br>
                <div class="text-center">
                    <div id="buttons" class="btn-group">
                        <input type="submit" value="Submit" class="btn btn-danger" style="width: 75px" onclick="return validate(this.form)"></input>
                        <input type="reset" value="Reset" class="btn btn-success" style="width: 75px" onclick="return confirm('Are you sure?')"></input>
                    </div>
                </div>
        </form>
    </main>
        
    <script>
    // javascript for disabling form submissions if there are invalid fields. Provided by Marc Javier
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
    <script>
        //checks if username already exists in DB->gets passed the username string as argument
        function checkUsername(str)
        {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() 
            {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById("Msg").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "username_check.php?q=" + str, true);
            xmlhttp.send();
        }
    </script>
</html>
<?php
    include ("footer.php");
?>