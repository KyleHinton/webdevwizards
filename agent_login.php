<!-- Author: Graeme Hodgkinson -->
<?php
    define ("TITLE","Agent Login");
    include ("header.php");
?>
<html>
    <head>
        
    </head>
    <style>

    main {
        background:white;
        color:black;
        padding:10px;
    }

    form {
        text-align:left;        
    }

    h3 {
        margin: 0 auto;
    }
    </style>
    
    <main>
        <br><br><br>
        <h3 style="text-align: center"> Agent Login </h3><br>
        <form class="needs-validation" id="myForm" method="post" action="LoginFileCheck.php" novalidate>
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Username</span>
                        </div>
                        <input type="text" name="name" class="form-control" id="p1" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Username" required>
                        <div class="invalid-feedback">Please provide a valid username.</div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Password</span>
                        </div>
                        <input type="password" name="password" class="form-control" id="p1" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Password" required>
                        <div class="invalid-feedback">Please provide a password.</div>
                    </div>  
                </div>
            </div>
            
            <div class="text-center">
                <div id="buttons" class="btn-group">
                    <input type="submit" value="Submit" class="btn btn-danger" style="width: 75px" onclick="return validate(this.form)"></input>
                    <input type="reset" value="Reset" class="btn btn-success" style="width: 75px" onclick="return confirm('Are you sure?')"></input>
                </div>
            </div>
            <br><br><br> <br><br><br><br><br><br>          
        </form>
    </main>
    <script>
    // javascript for disabling form submissions if there are invalid fields. --> Provided by Marc Javier
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
</html>
    <?php include "footer.php" ?>