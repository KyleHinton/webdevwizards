<!-- Created By: Marc Javier -->
<?php
define ("TITLE","Vacation Packages");
include ("header.php");
?>

<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="packages_style.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Dynamic tab script taken from jQueryUI -->
  <script>
  $(function() {
    $("#tabs").tabs({
      beforeLoad: function(event, ui) {
        ui.jqXHR.fail(function() {
          ui.panel.html(
            "Couldn't load this tab. We'll try to fix this as soon as possible. " );
        });
      }
    });
  } );
  </script>
</head>

<body>
<h2>Vacation Packages</h2>
<p>Whatever your taste may be, beach, city, the luxurious or the value-packed, we've got the perfect getaway for you.<br>
Check out some of our awesome cheap vacation package deals here, or call a Travel Experts consultant at 1 403 271 9873, ready to help you 24/7.</p>

<div id="tabs">
  <ul>
    <li><a href="packages_latest.php">Latest Deals</a></li>
    <li><a href="packages_missed.php">Missed Deals</a></li>
    <li><a href="packages_upcoming.php">Upcoming Deals</a></li>
  </ul>
</div>

</body>
</html>