<?php

/*----------------------------------------------*/
/*Author: Kyle Hinton							*/
/*Inspiration: Marc Javier						*/
/*Student ID: 000776182							*/
/*E-mail: kyle.hinton@edu.sait.ca				*/
/*-----------------------------------------------*/
//This script will get the agency contact information and display it in a table.

//Variables for mysqli server

include("header.php");
$server = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";
$agentdb = "(AgentId, AgtFirstName, AgtMiddleInitial, AgtLastName, AgtBusPhone, AgtEmail, AgtPosition, AgencyId)";
$customerdbi = "(CustomerId, CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustCountry, CustHomePhone, CustBusPhone, CustEmail)";

//Creating a connection objection
//$mysqli = new mysqli($server, $username, $password, $dbname);
$customersqli = new mysqli($server, $username, $password, $dbname);

echo		"<form action=\"TravelAgentUI.php\">";
echo    "<input type=\"submit\" value=\"Return to Travel Agent Screen\"/></form>";

	
if($customersqli-> connect_error){
		die("Error number: " . $customersqli->connect_error); //exit if there is a connection error. print the error message.
	}	
	
	else {echo "Connection to Agencies and Agents Successful!";};
//get the query from the agencies table
$key = $customerdbi;
$query = "SELECT * FROM customers";
//check if query matches
$result = mysqli_query($customersqli, $query) or die("There was a query error");

//if so, create a table
if($result){
$agtId = 0;
$offset = 0;
echo
'<table border "1">
	<tr>
		<td>CustomerId</td>
		<td>First Name</td>
		<td>Last Name</td>
		<td>Address</td>
		<td>City</td>
		<td>Province</td>
		<td>Postal Code</td>
		<td>Country</td>
		<td>Phone Number</td>
		<td>Bus Phone</td>		
		<td>E-mail</td>
		<td>Agent Id</td>
	</tr>';


while($theline = $result->fetch_assoc()){


echo 	'<tr>';
echo	'<td>' . $theline["CustomerId"] . '</td>';
echo	'<td>' . $theline["CustFirstName"] . '</td>';
echo	'<td>' . $theline["CustLastName"] . '</td>';
echo	'<td>' . $theline["CustAddress"] . '</td>';
echo	'<td>' . $theline["CustCity"] . '</td>';
echo	'<td>' . $theline["CustProv"] . '</td>';
echo	'<td>' . $theline["CustPostal"] . '</td>';
echo	'<td>' . $theline["CustCountry"] . '</td>';
echo	'<td>' . $theline["CustHomePhone"] . '</td>';
echo	'<td>' . $theline["CustBusPhone"] . '</td>';
echo	'<td>' . $theline["CustEmail"] . '</td>';
echo	'<td>' . $theline["AgentId"] . '</td>';
echo	'</tr>';
};
echo "</table>";
}else {
	echo "Nothing!";
}
	
?>
