 <!-- Created by Graeme Hodgkinson and Kyle Hinton -->
<?php	
    //$data is an object populated with input form data, used for sql query VALUES. 
	function insert2($data, $table_name, $columns)
	{		
		$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
		if ($conn->connect_error) 
		{
			die("connection failed: " . $conn->connect_error);
		}
			
		$values = $data->__toString();			
		$sql = "INSERT INTO $table_name "  . $columns . " VALUES ($values)";	        
        
        if ($conn->query($sql))
        {
            echo "Insert Successful";
        }
        else
        {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }        
		$conn->close();						
	}	
?>