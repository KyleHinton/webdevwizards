<!-- Author: Graeme Hodgkinson. This Page was previously an html made by Kyle Hinton, using the createData Function, it added agents to the database-->
<?php     
    include("function.php");
    include("header.php");
?>

<style>

main {
    background:white;
    color:black;
    padding:10px;
}

form {
    text-align:left;        
}

#buttons {    
    center
}

</style>
<main>
    <h3 style="text-align: center"> Agent Registration </h3><br>
    <form class="needs-validation" action="insert_agent.php" novalidate method="post">
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">First Name</span>
                    </div>
                    <input type="text" name="AgtFirstName" class="form-control" id="p1" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="First Name" required>
                    <div class="invalid-feedback">Please provide a valid name.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Middle Initial</span>
                    </div>
                    <input type="text" name="AgtMiddleInitial" class="form-control" id="p1" maxlength="3" pattern="^[A-Za-z -]+$" placeholder="Initial" required>
                    <div class="invalid-feedback">Please provide a valid initial.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Last Name</span>
                    </div>
                    <input type="text" name="AgtLastName" class="form-control" id="" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Last Name" required>
                    <div class="invalid-feedback">Please provide a valid name.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Phone Number</span>
                    </div>
                    <input type="text" name="AgtBusPhone" class="form-control" id="" pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$" placeholder="Phone" required>
                    <div class="invalid-feedback">Please provide a valid phone number.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Email Address</span>
                    </div>
                    <input type="email" name="AgtEmail" class="form-control" id="p10" placeholder="example@demo.com" required>
                    <div class="invalid-feedback">Please provide a valid email.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Agent Position</span>
                    </div>
                    <input type="text" name="AgtPosition" class="form-control" id="" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Position" required>
                    <div class="invalid-feedback">Please provide a valid position.</div>
                </div>  
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Agency Number</span>
                    </div>
                    <input type="text" name="AgencyId" class="form-control" id="" maxlength="5" pattern="^[0-9]+$" placeholder="Agency" required>
                    <div class="invalid-feedback">Please provide a valid agency.</div>
                </div>  
            </div>
        </div>

        <div class="text-center">
            <div id="buttons" class="btn-group">
                <input type="submit" value="Submit" class="btn btn-danger" style="width: 75px" onclick="return validate(this.form)"></input>
                <input type="reset" value="Reset" class="btn btn-success" style="width: 75px" onclick="return confirm('Are you sure?')"></input>
            </div>
        <div>

    </form>
    <br>
</main>

<script>
// javascript for disabling form submissions if there are invalid fields --> Via Marc Javier
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<?php
    include ("footer.php");
?>
</body>