<!-- Author: Graeme Hodgkinson -->
<?php
	class Customer 
	{
        //Fields match table columns in database
		private $CustFirstName;			
		private $CustLastName;
		private $CustAddress;
		private $CustCity;
		private $CustProv;
		private $CustPostal;
		private $CustCountry;
		private $CustHomePhone;			
		private $CustBusPhone;
		private $CustEmail;  
        private $Username;
        private $Password;
			
		public function getCustFirstName ()
		{
			return $this->CustFirstName;
		}
			
		public function setCustFirstName ($CustFirstName)
		{
			$this->CustFirstName = $CustFirstName;
		}			
				
		public function getCustLastName ()
		{
			return $this->CustLastName;
		}
			
		public function setCustLastName ($CustLastName)
		{
			$this->CustLastName = $CustLastName;
		}
			
		public function getCustAddress ()
		{
			return $this->CustAddress;
		}
			
		public function setCustAddress ($CustAddress)
		{
			$this->CustAddress = $CustAddress;
		}			
				
		public function getCustCity ()
		{
			return $this->CustCity;
		}
			
		public function setCustCity ($CustCity)
		{
			$this->CustCity = $CustCity;
		}			
			
		public function getCustProv ()
		{
			return $this->CustProv;
		}
			
		public function setCustProv ($CustProv)
		{
			$this->CustProv = $CustProv;
		}			
			
		public function getCustPostal ()
		{
			return $this->CustPostal;
		}
			
		public function setCustPostal ($CustPostal)
		{
			$this->CustPostal = $CustPostal;
		}			
			
		public function getCustCountry ()
		{
			return $this->CustCountry;
		}
		
		public function setCustCountry ($CustCountry)
		{
			$this->CustCountry = $CustCountry;
		}			
			
		public function getCustHomePhone ()
		{
			return $this->CustHomePhone;
		}
			
		public function setCustHomePhone($CustHomePhone)
		{
			$this->CustHomePhone = $CustHomePhone;
		}			
			

		public function getCustBusPhone ()
		{
			return $this->CustBusPhone;
		}
			
		public function setCustBusPhone ($CustBusPhone)
		{
			$this->CustBusPhone = $CustBusPhone;
		}
			
		public function getCustEmail ()
		{
			return $this->CustEmail;
		}
			
		public function setCustEmail ($CustEmail)
		{
			$this->CustEmail = $CustEmail;
		} 

        public function getUsername ()
		{
			return $this->Username;
		}
			
		public function setUsername ($Username)
		{
			$this->Username = $Username;
		}
        
        public function getPassword ()
		{
			return $this->Password;
		}
			
		public function setPassword ($Password)
		{
			$this->Password = $Password;
		}
        
        //Returns class fields as string formatted for sql insert: COLUMNS
		public function sqlColNames()
		{
			$property_array = (get_object_vars($this));	
            $property_string = implode(", ", array_keys($property_array));    
            $property_sql = "(" . $property_string . ")";
            return $property_sql;
        }          
			
		//Returns object properties as string, formatted for sql insert: VALUES
		public function __toString()
		{
			return "'" . $this->CustFirstName . "', '" . $this->CustLastName . "', '" . $this->CustAddress . "', '" . $this->CustCity . "', '" . $this->CustProv . "', '" . $this->CustPostal . "', '" . $this->CustCountry . "', '" . $this->CustHomePhone . "', '" . $this->CustBusPhone . "', '" . $this->CustEmail . "', '" . $this->Username . "', '" . $this->Password . "'";
		}
		
        //Receives $_POST array as argument
		public function __construct($array)
		{
            //DB connection required to use my_sqli_real_escape_string, to protect from SQL injection
			$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
			if ($conn->connect_error) 
			{
				die("connection failed: " . $conn->connect_error);
			}
            
			$keys = array_keys($array);
			if(array_key_exists('first', $array))
            {
                $this->setCustFirstName(mysqli_real_escape_string($conn, $array['first']));
            }
            if(array_key_exists('last', $array))
            {
                $this->setCustLastName(mysqli_real_escape_string($conn, $array['last']));
            }
            if(array_key_exists('address', $array))
            {
                $this->setCustAddress(mysqli_real_escape_string($conn, $array['address']));
            }
            if(array_key_exists('city', $array))
            {
                $this->setCustCity(mysqli_real_escape_string($conn, $array['city']));
            }
            if(array_key_exists('province', $array))
            {
                $this->setCustProv(mysqli_real_escape_string($conn, $array['province']));
            }
            if(array_key_exists('postal', $array))
            {
                $this->setCustPostal(mysqli_real_escape_string($conn, strtoupper($array['postal'])));
            }
            if(array_key_exists('country', $array))
            {
                $this->setCustCountry(mysqli_real_escape_string($conn, $array['country']));
            }
            if(array_key_exists('home#', $array))
            {
                $this->setCustHomePhone(mysqli_real_escape_string($conn, $array['home#']));
            }
            if(array_key_exists('bus#', $array))
            {
                $this->setCustBusPhone(mysqli_real_escape_string($conn, $array['bus#']));
            }
            if(array_key_exists('email', $array))
            {
                $this->setCustEmail(mysqli_real_escape_string($conn, $array['email']));  
            }
            if(array_key_exists('username', $array))
            {
                $this->setUsername(mysqli_real_escape_string($conn, $array['username']));  
            }
            if(array_key_exists('password', $array))
            {
                $this->setPassword(mysqli_real_escape_string($conn, password_hash($array['password'], PASSWORD_DEFAULT)));  
            }
            
			$conn->close();	
		}
	}
?>