<!-- Designed and Coded By:  Carol Bing He -->
<!-- part of the code is from Graeme Hodgkinson for Register modal---------->

<!--the following code is done by Carol Bing He	-------------->
	<?php
		define ("TITLE","Travel Experts");
		include ("header.php");
	?>

	<style>
		#div2 {
			background:#dbdee0;
			height:50px;
		}
		#div3 {
		
		}
		#div4 {
			height:50px;
			padding:10px;
			text-align:center;
			background:#dbdee0;
		}

		#div4 p {
			color:#5d6675;
			font-weight:bold;
			font-size:110%;
		}

		#div1 p{
			position: absolute;
			bottom: 0;
			background: rgba(0, 0, 0, 0.5); /* Black background with transparency */
			color: rgba(255,255,255,0.5);
			width: 30%;
			margin:0 auto;
			padding:10px;
			text-align:center;
			font-size:1.5vw;
		}
		#div1 {
			position:relative;
			text-align:center;
		}
		.nav-link:hover {
			background:#dbdee0;
		}

		#div5 .col {
			margin:5%;
		}
		
		      .modal {
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%; 
            height: 100%;             
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
        }
        
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            text-align:center;
        }

		.responsive {
			width: 100%;
			height: auto;
		}

	</style>
		<main>
		
<!--the above code is done by Carol Bing He ----------------------------------------------------------------------> 		
		
<!--the following code is done by Graeme  Hodgkinson--> 		
<!-- Register Modal -->    
        <div id="myModal" class="modal">   
            <div class="modal-dialog modal-sm">
                <div class="modal-content">            
                    <button type="button" class="close" data-dismiss="modal">&times;</button> 
                    <p>Thank you for registering with Travel Experts!</p>
                </div>
            </div>
        </div>
        
        <!--trigger modal if user just registered-->
        <?php
            if (isset($_GET['confirm']) && 1 == $_GET['confirm']) 
            {
                echo "
                <script>
                    var modal = document.getElementById('myModal');
                    modal.style.display = 'block';
                </script>
                ";
            }
        ?> 
<!--the above code is done by Graeme Hodgkinson--------------------------------------------------------------------->		

<!--the following code is done by Carol Bing He----------------------------------------------------------------------> 		
			
		<div id="div1">
			<!--display sliding show and welcome message-->
			<img class="slides" src="images/1920-1080-5.jpg" style="width:100%;height:350px;">
			<img class="slides" src="images/1920-1080-6.jpg" style="width:100%;height:350px;">
			<img class="slides" src="images/1920-1080-1.jpg" style="width:100%;height:350px;">
			<p id="greeting">      
						Welcome to Travel Experts <br>
				Check Our hot deals and packages below
			</p>
		</div>
		<div id="div2"></div><br>
		
		<!--bootstrap dynamic tab style is used here for creating flight/hotel/car booking section-->
		<!--it is for customer to access booking easily and quickly-->
		<div id="div3" class="container">
			<div>
				
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="flight" aria-selected="true">Flight</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="hotel" aria-selected="false">Hotels</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link" id="car-rental-tab" data-toggle="tab" href="#car-rental" role="tab" aria-controls="car-rental" aria-selected="false">Car Rental</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" id="cruise-tab" data-toggle="tab" href="#cruise" role="tab" aria-controls="cruise" aria-selected="false">Cruise</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" id="all-inclusive-tab" data-toggle="tab" href="#all-inclusiv" role="tab" aria-controls="all-inclusive" aria-selected="false">All Inclusive</a>
						</li>
					</ul><br><br>

					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
							<img src="images/capture_1.jpg" class="responsive">
						</div>

						<div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
							<img src="images/capture_2.jpg" class="responsive">
						</div>

						<div class="tab-pane fade" id="car-rental" role="tabpanel" aria-labelledby="car-rental-tab">
							<img src="images/capture_4.jpg" class="responsive">
						</div>

						<div class="tab-pane fade" id="cruise" role="tabpanel" aria-labelledby="cruise-tab">
							<img src="images/capture_5.jpg" class="responsive">
						</div>

						<div class="tab-pane fade" id="all-inclusiv" role="tabpanel" aria-labelledby="in-inclusive-tab">
							<img src="images/capture_3.jpg" class="responsive">
						</div>
					</div>
			</div>
		</div>
		<div id="div4"><p class="text-info">Choose Our Packages</p></div>
		
		<!--bootstrap cards style is used here to display package info on the index page-->
		<div id="div5" class="container">

			<div class="row">
				<div class="card col" style="width: 18rem;">
					<img class="card-img-top" src="images/1920-1080-6.jpg" alt="Card image cap">
					<div class="card-body">
						<h5 class="card-title">Caribbean New Year</h5>
						<p class="card-text">Departure: 2018-08-08<br>
											Returns: 2018-08-20<br>
											Cruise the Caribbean &amp; Celebrate the New Year...</p>
						<span class="btn btn-primary">$4800</span> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<a href="packages.php" class="btn btn-primary" align="right">See Details</a>
					</div>
				</div>

				<div class="card col" style="width: 18rem;">
					<img class="card-img-top" src="images/1920-1080-5.jpg" alt="Card image cap">
					<div class="card-body">
						<h5 class="card-title">Polynesian Paradise</h5>
						<p class="card-text">
											Departure: 2018-08-01<br>

											Returns: 2018-08-09<br>

											8 Day All Inclusive Hawaiian Vacation...

						</p>
						<span class="btn btn-primary">$3000</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <a href="packages.php" class="btn btn-primary" align="right">See Details</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="card col" style="width: 18rem;">
					<img class="card-img-top" src="images/1920-1080-1.jpg" alt="Card image cap">
					<div class="card-body">
						<h5 class="card-title">Asian Expedition</h5>
						<p class="card-text">
							Departure: 2018-09-12<br>

							Returns: 2018-09-22<br>

							Airfaire, Hotel and Eco Tour...

						</p>
						<span class="btn btn-primary">$2800</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <a href="packages.php" class="btn btn-primary" align="right">See Details</a>
					</div>
				</div>

				<div class="card col" style="width: 18rem;">
					<img class="card-img-top" src="images/1920-1080-4.jpg" alt="Card image cap">
					<div class="card-body">
						<h5 class="card-title">European Vacation</h5>
						<p class="card-text">
							Departure: 2018-09-18<br>

							Returns: 2018-09-30<br>

							Euro Tour with Rail Pass and Travel Insurance

						</p>
						<span class="btn btn-primary">$3000</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <a href="packages.php" class="btn btn-primary" align="right">See Details</a>
					</div>
				</div>
			</div>



		

		</div>

	</main>
	
<!--the above code is done by Carol Bing He--------------------------------------------------------------------->		

<!--the following code is done by Graeme Hodgkinson---------------------------------------------------------------------->
	  <script>
    // Get the modal
    var modal = document.getElementById('myModal');        

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    </script>
	
<!--the above code is done by Graeme Hodgkinson--------------------------------------------------------------------->		

<!--the following code is done by Carol Bing He---------------------------------------------------------------------->
	
	<script>
		//this is for sliding show effect
		var index = 0;
		sliding();

		function sliding() {
			var i;
			var x = document.getElementsByClassName("slides");
			for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";  
			}
			index++;
			if (index > x.length) {index = 1}    
			x[index-1].style.display = "block";  
			setTimeout(sliding, 5000); // Change image every 5 seconds
		}

	

	
			
	</script>

	<?php
		include ("footer.php");
	?>