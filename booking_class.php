<!-- Author: Graeme Hodgkinson -->
<?php
    class Booking
    {
        //Fields match table columns in database
        private $BookingDate;
        private $BookingNo;
        private $TravelerCount;
        private $TripTypeId;
        private $PackageId;
        private $CustomerId;
        
        public function getBookingDate ()
		{
			return $this->BookingDate;
		}
			
		public function setBookingDate ($BookingDate)
		{
			$this->BookingDate = $BookingDate;
		}	

        public function getBookingNo ()
		{
			return $this->BookingNo;
		}
			
		public function setBookingNo ($BookingNo)
		{
			$this->BookingNo = $BookingNo;
		}		
				
		public function getTravelerCount ()
		{
			return $this->TravelerCount;
		}
			
		public function setTravelerCount ($TravelerCount)
		{
			$this->TravelerCount = $TravelerCount;
		}
			
		public function getTripTypeId ()
		{
			return $this->TripTypeId;
		}
			
		public function setTripTypeId ($TripTypeId)
		{
			$this->TripTypeId = $TripTypeId;
		}	
        
        public function getPackageId ()
		{
			return $this->PackageId;
		}
			
		public function setPackageId ($PackageId)
		{
			$this->PackageId = $PackageId;
		}	
        
        public function getCustomerId ()
		{
			return $this->CustomerId;
		}
			
		public function setCustomerId ($CustomerId)
		{
			$this->CustomerId = $CustomerId;
		}	
        
        //Returns class fields as string formatted for sql insert: COLUMNS
        public function sqlColNames()
		{
			$property_array = (get_object_vars($this));	
            $property_string = implode(", ", array_keys($property_array));    
            $property_sql = "(" . $property_string . ")";
            return $property_sql;
        }
        
        //Returns object properties as string, formatted for sql insert: VALUES
        public function __toString()
		{
			return "'" . $this->BookingDate . "', '" . $this->BookingNo . "', '" . $this->TravelerCount . "', '" . $this->TripTypeId . "', '" . $this->PackageId . "', '" . $this->CustomerId . "'";
		}
        
        //Receives $_POST array as argument
		public function __construct($array)
		{
            //DB connection required to use my_sqli_real_escape_string, to protect from SQL injection
			$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
			if ($conn->connect_error) 
			{
				die("connection failed: " . $conn->connect_error);
			}
            
			$keys = array_keys($array);            
            if(array_key_exists('BookingDate', $array))
            {
                $this->setBookingDate(mysqli_real_escape_string($conn, $array['BookingDate']));
            }
            if(array_key_exists('BookingNo', $array))
            {
                $this->setBookingNo(mysqli_real_escape_string($conn, $array['BookingNo']));
            }
            if(array_key_exists('TravelerCount', $array))
            {
                $this->setTravelerCount(mysqli_real_escape_string($conn, $array['TravelerCount']));
            }
            if(array_key_exists('TripTypeId', $array))
            {
                $this->setTripTypeId(mysqli_real_escape_string($conn, $array['TripTypeId']));	
            }
            if(array_key_exists('PackageId', $array))
            {
                $this->setPackageId(mysqli_real_escape_string($conn, $array['PackageId']));	
            }
			if(array_key_exists('CustomerId', $array))
            {
                $this->setCustomerId(mysqli_real_escape_string($conn, $array['CustomerId']));
            }                
			$conn->close();	
		}
    }
?>