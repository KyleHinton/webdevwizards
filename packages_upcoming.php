<!-- Created By: Marc Javier -->
<!-- Display packages with start dates starting after 12 months of local time -->

<?php

//Credentials
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";

//Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//Check connection
if (!$conn) {
trigger_error(mysqli_connect_error());
// } else {
// echo("Connection ok!<br />");
}

// $key = "(PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice)";
$query = "SELECT * FROM packages 
        WHERE PkgStartDate > ADDDATE(LOCALTIME, INTERVAL 12 MONTH) 
        ORDER BY PkgStartDate ASC";

$result = mysqli_query($conn, $query) or die("SQL Error");

echo '<div class="container">';
echo '<div class="row">';

if ($result) {
    while($row = $result -> fetch_assoc()) {
    echo "
    <div class='col-lg-8' id='banner'>
    <img class='package-logo' src='images/packages/${row['PackageId']}.jpeg' alt='${row['PkgDesc']}' height='125px' width='125px'>
    <div class='package-price'>$${row['PkgBasePrice']}<br /></div>
    <div class='package-title'>${row['PkgName']}</div>
    <div class='package-start'><i class='far fa-calendar-alt'></i>Departs: ${row['PkgStartDate']}<br /></div>
    <div class='package-end'><i class='far fa-calendar-alt'></i>Returns: ${row['PkgEndDate']}<br /></div>
    <div class='package-book'><button type='button' class='btn btn-dark'>Notify Me</button><br /></div>
    <div class='package-desc'>${row['PkgDesc']}</div>
    </div>";
    }

    echo '</div>';
    echo '</div>';

} else {
echo "0 results";
}

//Close connection
mysqli_close($conn);