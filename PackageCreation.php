<!--Author: Kyle Hinton and Graem Hodgkinson-->
<?php
	define("TITLE", "Package Creation");
	include("header.php");
	?>
	
<html>
    <style>

    main {
        background:#ffffde;
        color:black;
        padding:10px;
    }

    form {
        text-align:left;        
    }

    h3 {
        margin: 0 auto;
    }

    #buttons {    
        center
    }

    </style>

<main>

<body>

</main>
</html>
    <h3 style="text-align: center"> Create Package </h3><br>
	<form class="needs-validation" action="CREATEPACKAGE.php" novalidate method="post">
		<div class = "form-group">
			<div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
				<div class="input-group">	
					<div class="input-group-prepend">
						<span class="input-group-text">PacckageId</span>
					</div>
					 <input type="text" name="PackageId" class="form-control" id="p1" maxlength="25" placeholder="PackageId" required>
	                 <div class="invalid-feedback">What is the Package Id?</div>
				</div>
            </div>
		</div>
		
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Package Start Date</span>
                    </div>
                    <input type="text" name="PkgStartDate" class="form-control" id="" placeholder="Start Date" required>
                    <div class="invalid-feedback">What is the Package Start Date?</div>
                </div>  
            </div>
        </div>					
					
		<div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Package End Date</span>
                    </div>
                    <input type="text" name="PkgEndDate" class="form-control" id="" placeholder="End Date" required>
                    <div class="invalid-feedback">What is the Package End Date?.</div>
                </div>  
            </div>
        </div>				
					
        <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Package Description</span>
                    </div>
                    <input type="text" name="PkgDesc" class="form-control" id="" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Description" required>
                    <div class="invalid-feedback">Please provide a valid entry.</div>
                </div>  
            </div>
        </div>					
					
		<div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Package Base Price</span>
                    </div>
                    <input type="text" name="PkgBasePrice" class="form-control" id="" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="The Base Price" required>
                    <div class="invalid-feedback">Please provide a valid entry.</div>
                </div>  
            </div>
        </div>	
		
       <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">                    
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Package Commission</span>
                    </div>
                    <input type="text" name="PkgAgencyDescription" class="form-control" id="" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Commision Price" required>
                    <div class="invalid-feedback">Please provide a valid entry.</div>
                </div>  
            </div>
        </div>
            <div id="buttons" class="btn-group">
                <input type="submit" value="Submit" class="btn btn-danger" style="width: 75px" onclick="return validate(this.form)"></input>
                <input type="reset" value="Reset" class="btn btn-success" style="width: 75px" onclick="return confirm('Are you sure?')"></input>
            </div>
			<div>
		</form>
		<br>
	</main>
	<script>
	(function() {
	//Made by Marc and Graem.
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
	</script>
		<?php include ("footer.php");?>
	</body>
	
	</html>

	
					