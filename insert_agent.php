 <!-- Created by Graeme Hodgkinson. Edited By Kyle Hinton --> 

	<?php
		$table_name = "agents";
		include "insert2.php";
		include_once "agent_class.php";
        $conn = new mysqli("localhost", "root", "", "travelexperts"); 			
		if ($conn->connect_error) 
		{
			die("connection failed: " . $conn->connect_error);
		}  
        
        $conn->close();
        if(isset($_REQUEST))
        {                
           $data = new Agent($_POST);                
        }
        $columns = $data->sqlColNames();
        insert2($data, $table_name, $columns);
        
        //redirect to index and enable modal popup
        header('Location: TravelAgentUI.php?confirm=1');   
        
	?>