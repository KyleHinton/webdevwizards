<!-- Created by: Marc Javier -->
<!-- DELETE THIS FILE -->
<?php
session_start();

$pid = $_GET['pid'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Order Form</title>
</head>

<body>
  <div>
    <!-- AREA FOR PACKAGE -->
<?php
//Credentials
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";

//Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//Check connection
if (!$conn) {
    trigger_error(mysqli_connect_error());
} else {
    echo("Connection ok!<br />");
}

$key = "(PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice)";
$query = "SELECT * FROM packages WHERE PackageId=$pid";

$result = mysqli_query($conn, $query) or die("SQL Error");
$row = $result -> fetch_assoc();

if ($result) {
  print_r($row);
}

//Close connection
mysqli_close($conn);
?>

  </div>

  <form action="process.php" method="post" onsubmit="return validate()">
    <fieldset class="passenger-information">
      <h3>Passenger Information</h3>
      <p>Enter exactly as stated on your passport to avoid future problems.<br>
        <span class="required">*</span>Required Fields</p>
      <table>
        <tr>
          <th>Title</th>
          <th>First Name<span class="required">*</span></th>
          <th>Middle Name</th>
          <th>Last Name<span class="required">*</span></th>
          <th>Date of Birth<span class="required">*</span></th>
        </tr>
        <tr>
          <td><select name="personal-title" id="personal-title">
            <option value="title-blank"></option>
            <option value="title-mister">Mr.</option>
            <option value="title-mrs">Mrs.</option>
            <option value="title-ms">Ms.</option>
          </select></td>
          <td><input type="text" name="first-name" id="first-name" placeholder="First Name" value="John"></td>
          <td><input type="text" name="mid-initial" id="mid-initial" placeholder="Middle Name" vlaue="X"></td>
          <td><input type="text" name="last-name" id="last-name" placeholder="Last Name" value="Doe"></td>
          <td><input type="date" name="date-of-birth" id="date-of-birth"></td>
          <td>[+]</td>
        </tr>
      </table>
    </fieldset>
    <fieldset class="contact-information">
      <h3>Contact Information</h3>
      <label for="street_address">Address</label><span class="required">*</span>
      <input type="text" name="street-address" id="street_address" value="123 Test St"><br>
      <label for="city_address">City</label><span class="required">*</span>
      <input type="text" name="city-address" id="city_address" value="Calgary">
      <label for="province_address">Province</label><span class="required">*</span>
      <select name="province-address" id="province_address" value="AB">
        <option disabled selected>Select Province</option><span class="required">*</span>
        <option value="AB">Alberta</option>
        <option value="BC">British Columbia</option>
        <option value="MB">Manitoba</option>
        <option value="NB">New Brunswick</option>
        <option value="NL">Newfoundland and Labrador</option>
        <option value="NS">Nova Scotia</option>
        <option value="NT">Northwest Territories</option>
        <option value="NU">Nunavut</option>
        <option value="ON">Ontario</option>
        <option value="PE">Prince Edward Island</option>
        <option value="QC">Quebec</option>
        <option value="SK">Saskatchewan</option>
        <option value="YT">Yukon Territories</option>
      </select><br>
      <label for="postal-code">Postal Code</label><span class="required">*</span>
      <input type="text" name="postal-code-address" id="postal_code_address" value="T1Y 1Y1">
      <label for="country-address">Country</label><span class="required">*</span>
      <select name="country-address" id="country-address" value="Canada">
        <option value="CANADA" selected>Canada</option>
      </select>
      <p>
      <label for="contact-phone">Home Phone Number</label><span class="required">*</span>
      <input type="text" name="contact-home-phone" id="contact_home_phone" value="1234567890">
      <label for="contact-phone">Business Phone Number</label><span class="required">*</span>
      <input type="text" name="contact-bus-phone" id="contact_bus_phone" value="1234567890">
      <label for="email-address">Email Address</label><span class="required">*</span>
      <input type="email" name="contact-email" id="email-address" value="john@doe.com">
    </fieldset>
    <fieldset class="payment-information">
      <h3>Payment Information</h3>
      <label for="cc-number">Card Number</label><span class="required">*</span>
      <input type="tel" name="order-card-number" value="5555555555555555"><br>
      <label for="cc-expiry">Expiry Date</label><span class="required">*</span>
      <input type="text" name="order-card-expiry" value="">
      <label for="cvv">Security Number</label><span class="required">*</span>
      <input type="text" name="order-card-security"><br>
      <label for="cc-name">Cardholder Name</label><span class="required">*</span>
      <input type="text" name="order-card-name">
    </fieldset>
    <div class="booking-certify">
      <input type="checkbox" name="booking-certify" id="">
      <label for="terms-and-conditions">I certify that the above information is correct...</label><span class="required">*</span>
    </div>
    <div class="terms-and-conditions">
      <input type="checkbox" name="booking-terms" id="terms-and-conditions">
      <label for="terms-and-conditions">I agree with Travel Experts Terms & Conditions...</label><span class="required">*</span>
    </div>
    <div class="controls">
      <button type="#">Back</button>
      <button type="submit">Complete Booking</button>
    </div>
  
  </form>

</body>
</html>

