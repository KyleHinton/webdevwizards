<!-- Author: Graeme Hodgkinson -->
<?php
    class Credit_Card
    {
        //Fields match table columns in database
        private $CCName;
        private $CCNumber;
        private $CCExpiry;
        private $CustomerId;
        
        public function getCCName ()
		{
			return $this->CCName;
		}
			
		public function setCCName ($CCName)
		{
			$this->CCName = $CCName;
		}			
				
		public function getCCNumber ()
		{
			return $this->CCNumber;
		}
			
		public function setCCNumber ($CCNumber)
		{
			$this->CCNumber = $CCNumber;
		}
			
		public function getCCExpiry ()
		{
			return $this->CCExpiry;
		}
			
		public function setCCExpiry ($CCExpiry)
		{
			$this->CCExpiry = $CCExpiry;
		}	
        
        public function getCustomerId ()
		{
			return $this->CustomerId;
		}
			
		public function setCustomerId ($CustomerId)
		{
			$this->CustomerId = $CustomerId;
		}	
        
        //Returns class fields as string formatted for sql insert: COLUMNS
        public function sqlColNames()
		{
			$property_array = (get_object_vars($this));	
            $property_string = implode(", ", array_keys($property_array));    
            $property_sql = "(" . $property_string . ")";
            return $property_sql;
        }
        
        //Returns object properties as string, formatted for sql insert: VALUES
        public function __toString()
		{
			return "'" . $this->CCName . "', '" . $this->CCNumber . "', '" . $this->CCExpiry . "', '" . $this->CustomerId . "'";
		}
        
        //Receives $_POST array as argument
		public function __construct($array)
		{
            //DB connection required to use my_sqli_real_escape_string, to protect from SQL injection
			$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
			if ($conn->connect_error) 
			{
				die("connection failed: " . $conn->connect_error);
			}				
            
			$keys = array_keys($array);			
            if(array_key_exists('CCName', $array))
            {
                $this->setCCName(mysqli_real_escape_string($conn, $array['CCName']));
            }
			if(array_key_exists('CCNumber', $array))
            {
                $this->setCCNumber(mysqli_real_escape_string($conn, $array['CCNumber']));
            }
            if(array_key_exists('CCExpiry', $array))
            {
                $this->setCCExpiry(mysqli_real_escape_string($conn, $array['CCExpiry'] . "-01"));
            }                
			if(array_key_exists('CustomerId', $array))
            {
                $this->setCustomerId(mysqli_real_escape_string($conn, $array['CustomerId']));
            }   
			$conn->close();	
		}
    }
?>