<!--*****************************************************
***    PROJ 207B THREADED PROJECT OOSD SPRING2018    ****
***    ORDER FORM                                    ****
***    CODED BY: MARC JAVIER                         ****
******************************************************-->

<?php
		define ("TITLE","Order Package");
		include ("header.php");
        include ("random_string.php");
        $pid = $_GET['pid'];    // grabs package variable from packages page
        
?>

<!-- style by: Graeme Hodgkinson -->
<style>
  main {
    background: white;
    color: black;
  }
  p {
      margin-bottom:10px;
  }
   
</style>

<main>
  <form class="needs-validation" action="insert_order_form.php" method="post" novalidate>
    <h2 style="text-align: center; text-decoration: underline">Order</h2>
    <p style="text-align:center"><?php include "get_package_name.php"?></p>
    <fieldset class="passenger-information" id="passenger-information">
      <h5 style="text-align: center; text-decoration: underline"> Customer Information </h5>
        <p style="text-align:center">Please enter the following information</p>
        <p style="text-align:center">* Required</p>
                   
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">First Name*</span>
                        </div>
                        <input type="text" name="first" class="form-control" id="p1" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="First Name" required>
                        <div class="invalid-feedback">Please provide a valid name.</div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Last Name*</span>
                        </div>
                        <input type="text" name="last" class="form-control" id="p2" maxlength="25" pattern="^[A-Za-z -]+$" placeholder="Last Name" required>
                        <div class="invalid-feedback">Please provide a valid name.</div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Address*</span>
                        </div>
                        <input type="text" name="address" class="form-control" id="p3" maxlength="75" placeholder="Address" required>
                        <div class="invalid-feedback">Please provide a valid address.</div>
                    </div>  
                </div>
            </div>   
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">City*</span>
                        </div>
                        <input type="text" name="last" class="form-control" id="p2" maxlength="50" pattern="^[A-Za-z -]+$" placeholder="City" required>
                        <div class="invalid-feedback">Please provide a valid city.</div>
                    </div>  
                </div>
            </div>     
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Province*</span>
                        </div>
                        <select class="custom-select" name="province" id="p5" required>
                        <option disabled selected>Select Province</option>
                        <option value="AB">Alberta</option>
                        <option value="BC">British Columbia</option>
                        <option value="MB">Manitoba</option>
                        <option value="NB">New Brunswick</option>
                        <option value="NL">Newfoundland and Labrador</option>
                        <option value="NS">Nova Scotia</option>
                        <option value="NT">Northwest Territories</option>
                        <option value="NU">Nunavut</option>
                        <option value="ON">Ontario</option>
                        <option value="PE">Prince Edward Island</option>
                        <option value="QC">Quebec</option>
                        <option value="SK">Saskatchewan</option>
                        <option value="YT">Yukon Territories</option>
                    </select>
                        <div class="invalid-feedback">Please provide a valid province.</div>
                    </div>  
                </div>
            </div>   

            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Postal Code*</span>
                        </div>
                        <input type="text" name="postal" class="form-control" id="p6" maxlength="7" pattern="[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz] ?[0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]" placeholder="ex. T2E 9B5 or T2E9B5" required>
                        <div class="invalid-feedback">Please provide a valid postal code.</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Country*</span>
                        </div>
                        <select class="custom-select" name="country" id="p7" required>
                            <option selected>Canada</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Home Phone*</span>
                        </div>
                        <input type="text" name="home#" class="form-control" id="p8" pattern="/\A[(]?[0-9]{3}[)]?[ ,-]?[0-9]{3}[ ,-]?[0-9]{4}\z/" placeholder="Home Phone" required>
                        <div class="invalid-feedback">Please provide a valid phone number.</div>
                    </div>  
                </div>
            </div>              
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Bus. Phone</span>
                        </div>
                        <input type="text" name="bus#" class="form-control" id="p9" pattern="/\A[(]?[0-9]{3}[)]?[ ,-]?[0-9]{3}[ ,-]?[0-9]{4}\z/" placeholder="Business Phone">
                        <div class="invalid-feedback">Please provide a valid phone number.</div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Email Address*</span>
                        </div>
                        <input type="email" name="email" class="form-control" id="p10" placeholder="example@demo.com" required>
                        <div class="invalid-feedback">Please provide a valid email.</div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Username*</span>
                        </div>
                        <input type="text" name="username" class="form-control" id="p8" pattern="[a-zA-Z0-9-]+" onblur="checkUsername(this.value)" placeholder="Username" required>
                        <div class="invalid-feedback">Please provide a valid username. Numbers and letters only.</div>
                        <div id="Msg"></div>
                    </div>  
                </div>
            </div>
            
            <div class="form-group" id="password">
                <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Password*</span>
                        </div>
                        <input type="password" name="password" class="form-control" id="p8" pattern="[a-zA-Z0-9-]+" placeholder="Password" required>
                        <div class="invalid-feedback">Please provide a valid password. Numbers and letters only.</div>
                    </div>  
                </div>
            </div>
    </fieldset>
    
    <fieldset class="payment-information">
      <h5 style="text-align: center; text-decoration: underline"> Payment Information </h5>
       

      <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Card Number*</span>
                    </div>
                    <input type="tel" maxlength="20" name="CCNumber" class="form-control" id="p8" onkeyup="validatecardnumber(this.value)" placeholder="Credit Card Number" required>
                    <div id="notice"></div>
                </div>  
            </div>
      </div>    
      

      <div class="form-group">
            <div class="col-xs-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Credit Card Expiry*</span>
                    </div>
                    <input type="text" maxlength="20" name="CCExpiry" class="form-control" id="" maxlength="7" placeholder="YYYY-MM" pattern="[0-9]{4}[-][0-9]{2}" required>
                    <div class="invalid-feedback">Please provide a valid expiry. Format: YYYY-MM.</div>
                </div>  
            </div>
      </div>    
      
    </fieldset>
    
    <!-- Static values to be stored in database along with package
    By: Graeme Hodgkinson -->
    <fieldset class="invisible" style="display: none;">
        <div class="form-group">
            <input type="invisible" name="ClassId" value="ECN">
        </div>
    
        <div class="form-group">
            <input type="invisible" name="TravelerCount" value="1">
        </div>
        
        <div class="form-group">
            <input type="invisible" name="TripTypeId" value="L">
        </div>
        
        <div class="form-group">
            <input type="invisible" name="FeeId" value="BK">
        </div>
        
        <div class="form-group">
            <input type="invisible" name="BookingNo" value="<?php echo randomString()?>">
        </div>
        
        <div class="form-group">
            <input type="invisible" name="BookingDate" value="<?php echo date("Y-m-d")?>">
        </div>
        
        <div class="form-group">
            <input type="invisible" name="PackageId" value="<?php echo $pid ?>">
        </div>
    </fieldset>

    <div id="last_div"><br>
        <div class="text-center">
            <input type="checkbox" name="booking-certify" id="" required>
            <label for="terms-and-conditions">I certify that the above information is correct.</label><span class="required">*</span>
        </div>
       
        <div class="text-center">
          <div class="terms-and-conditions">
            <input type="checkbox" name="booking-terms" id="terms-and-conditions" required>
            <label for="terms-and-conditions">I agree with Travel Experts terms &amp; conditions.</label><span class="required">*</span>
          </div>
        </div>
      <div class="text-center">
        <div id="buttons" class="btn-group">
            <input type="submit" value="Submit" class="btn btn-danger" style="width: 75px" onclick="return validate(this.form)"></input>
            <input type="reset" value="Reset" class="btn btn-success" style="width: 75px" onclick="return confirm('Are you sure?')"></input>
        </div>
      </div>
    </div>
  </form>
</main>

<script>
// JavaScript for disabling form submissions if there are invalid fields
// Script taken from Bootstrap Documentation
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function validatecardnumber(cardnumber) {
  // Strip spaces and dashes
  cardnumber = cardnumber.replace(/[ -]/g, '');
  // See if the card is valid
  // The regex will capture the number in one of the capturing groups
  var match = /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/.exec(cardnumber);
  if (match) {
    // List of card types, in the same order as the regex capturing groups
    var types = ['Visa', 'MasterCard', 'Discover', 'American Express', 'Diners Club', 'JCB'];
    // Find the capturing group that matched
    // Skip the zeroth element of the match array (the overall match)
    for (var i = 1; i < match.length; i++) {
      if (match[i]) {
        // Display the card type for that group
        document.getElementById('notice').innerHTML = types[i - 1];
        break;
      }
    }
  } else {
    document.getElementById('notice').innerHTML = '(invalid card number)';
  }
}

//checks if username already exists in DB
//Script provided by: Graeme Hodgkinson
function checkUsername(str)
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("Msg").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "username_check.php?q=" + str, true);
    xmlhttp.send();
}

</script>

<?php
		include ("footer.php");
?>