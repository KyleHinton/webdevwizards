<!-- Created By: Marc Javier -->
<!-- Displays outdated packages that is older than local time -->

<?php

//Credentials
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";

//Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//Check connection
if (!$conn) {
trigger_error(mysqli_connect_error());
// } else {
// echo("Connection ok!<br />");
}

// $key = "(PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice)";
$query = "SELECT * FROM packages 
        WHERE PkgEndDate < LOCALTIME() 
        ORDER BY PkgStartDate DESC";

$result = mysqli_query($conn, $query) or die("SQL Error");

echo '<div class="container">';
echo '<div class="row">';

if ($result) {
    while($row = $result -> fetch_assoc()) {
    $saleprice = $row['PkgBasePrice'] * .2;
    echo "
    <div class='col-lg-8' id='banner-missed'>
    <img class='package-logo' src='images/packages/${row['PackageId']}.jpeg' alt='${row['PkgDesc']}' height='125px' width='125px'>
    <div class='package-price-missed'>$${row['PkgBasePrice']}<br /></div>
    <div class='package-title'>${row['PkgName']}</div>
    <div class='package-price-sale'>SALE: $$saleprice<br /></div>
    <div class='package-start-missed'><i class='far fa-calendar-alt'></i>Departs: ${row['PkgStartDate']}<br /></div>
    <div class='package-end-missed'><i class='far fa-calendar-alt'></i>Returns: ${row['PkgEndDate']}<br /></div>
    <div class='package-book'><button class='btn btn-secondary'>BOOK</button><br /></div>
    <div class='package-desc'>${row['PkgDesc']}</div>
    </div>";
    }

    echo '</div>';
    echo '</div>';

} else {
echo "0 results";
}

//Close connection
mysqli_close($conn);