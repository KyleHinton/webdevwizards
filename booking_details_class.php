<!-- Author: Graeme Hodgkinson -->
<?php
    class BookingDetails
    {
        //Fields match table columns in database        
        private $BookingId;
        private $ClassId;
        private $FeeId;   
			
		public function getBookingId ()
		{
			return $this->BookingId;
		}
			
		public function setBookingId ($BookingId)
		{
			$this->BookingId = $BookingId;
		}	
        
        public function getClassId ()
		{
			return $this->ClassId;
		}
			
		public function setClassId ($ClassId)
		{
			$this->ClassId = $ClassId;
		}	
        
        public function getFeeId ()
		{
			return $this->FeeId;
		}
			
		public function setFeeId ($FeeId)
		{
			$this->FeeId = $FeeId;
		}	
        
        //Returns class fields as string formatted for sql insert: COLUMNS
        public function sqlColNames()
		{
			$property_array = (get_object_vars($this));	
            $property_string = implode(", ", array_keys($property_array));    
            $property_sql = "(" . $property_string . ")";
            return $property_sql;
        }
        
        //Returns object properties as string, formatted for sql insert: VALUES
        public function __toString()
		{
			return "'" . $this->BookingId . "', '" . $this->ClassId . "', '" . $this->FeeId . "'";
		}
        
        //Receives $_POST array as argument
		public function __construct($array)
		{
            //DB connection required to use my_sqli_real_escape_string, protects from SQL injection
			$conn = new mysqli("localhost", "root", "", "travelexperts"); 			
			if ($conn->connect_error) 
			{
				die("connection failed: " . $conn->connect_error);
			}	
			
			$keys = array_keys($array);  
            if(array_key_exists('BookingId', $array))
            {
                $this->setBookingId(mysqli_real_escape_string($conn, $array['BookingId']));	
            }
            if(array_key_exists('ClassId', $array))
            {
                $this->setClassId(mysqli_real_escape_string($conn, $array['ClassId']));	
            }
			if(array_key_exists('FeeId', $array))
            {
                $this->setFeeId(mysqli_real_escape_string($conn, $array['FeeId']));
            }                
			$conn->close();	
		}
    }
?>