// Scripts for Register.php. Created by Graeme Hodgkinson for OOSD JS-Module 7

function validate(form) 
{ 
	if (form.elements[0].value == "" || form.elements[0].value == null) 
	{
		alert("Please enter your first name!");
		form.elements[0].focus();
		return false;
	} 
	
	if (form.elements[1].value == "" || form.elements[1].value == null) 
	{
		alert("Please enter your last name!");
		form.elements[1].focus();
		return false;
	}
	
	if (form.elements[2].value == "" || form.elements[2].value == null) 
	{
		alert("Please enter an address!");
		form.elements[2].focus();
		return false;
	}
	
	if (form.elements[3].value == "" || form.elements[3].value == null) 
	{
		alert("Please enter your city!");
		form.elements[3].focus();
		return false;
	}
	
	if (form.elements[4].value == "" || form.elements[4].value == null) 
	{
		alert("Please enter your province!");
		form.elements[4].focus();
		return false;
	}
	
	// Postal code pattern
	var regex = /^[A-Z]\d[A-Z] ?\d[A-Z]\d$/i;
	if (regex.test(form.elements[5].value)  == false) 
	{
		alert("Please enter a valid postal code!"); 
		form.elements[5].focus();
		return false;
	} 
	
	if (form.elements[5].value == "")
	{
		alert("Please enter your postal code!");
		form.elements[5].focus();
		return false;
	}
	
	if (form.elements[6].value == "" || form.elements[6].value == null) 
	{
		alert("Please enter your country!");
		form.elements[6].focus();
		return false;
	}
	
	if (form.elements[7].value == "" || form.elements[7].value == null) 
	{
		alert("Please enter your home phone number!");
		form.elements[7].focus();
		return false;
	}
	
	if (form.elements[8].value == "" || form.elements[8].value == null) 
	{
		alert("Please enter your business phone number!");
		form.elements[8].focus();
		return false;
	}
	
	if (form.elements[9].value == "" || form.elements[9].value == null) 
	{
		alert("Please enter your email address!");
		form.elements[9].focus();
		return false;
	}
	
	if (form.elements[10].value == "" || form.elements[10].value == null) 
	{
		alert("Please enter a password!");
		form.elements[10].focus();
		return false;
	} 
	
	else {
	return true;
	} 
}
//form input hint-paragraph hide/show functions
function makeVisible(x) 
{ 
	document.getElementById(x+1).style.visibility = "visible"; 
}

function makeHidden(x) 
{ 
	document.getElementById(x+1).style.visibility = "hidden";
}