

<!-- Designed and Coded By:  Carol Bing He -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="css/fa-solid.min.css">
    <link rel="stylesheet" href="css/fontawesome.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<title>
        <?php
            if (defined("TITLE")) {
                print TITLE;
            }
        ?>
    </title>

    <style>
	
		* {
			box-sizing:border-box;
			font-family: Arial, Helvetica, sans-serif;
		}
		body {
			margin:0;
			padding-left:8%;
			padding-right:8%;
		}
		header div {
			display:flex;
			justify-content: space-around;
			flex-wrap: wrap;
		}
		header h2, header h3 {
			margin-top:5px;
			margin-bottom: 5px;
			font-size:125%;
		}

		#nav1 ul {
			background:#dbdee0;
			list-style: none;
			display:flex;
			justify-content: space-around;
			margin:0;
			padding:10px;
			font-weight: bold;
		}
		#nav1 li {
			width:12%;
			text-align:center;
		}
		#nav1 a {
			color:#50a8b7;
			text-decoration: none;
			display:block;
			width:100%;
			padding:5px 5px;
			font-size:110%;
			border-radius:5px;
		}


		#nav1 a:hover {
			background:#50a8b7;
			color:white;
			text-decoration: none;
		}

		#nav2 {
			display:none;
		}

        footer {
			background:#dbdee0;
			margin:0;
			padding:10px;
			text-align: center;
		}

		footer h5 {
			margin:auto;
			font-weight: bold;
		}

		@keyframes animate-h2 {
			0% {
				color: white;
			}

			20% {
				color:blue;
			}
			40% {
				color:red;
			}
			60% {
				color:yellow;
			}

			90% {
				color:white;
			}
		}

		@keyframes animate-phonenumber {
			0% {
				transform: translate(0,0);
			}

			20% {
				transform: translate(0, 1px);
			}
			40% {
				transform: translate(0, 2px);
			}

			60% {
				transform: translate(0, 1px);
			}
			90% {
				transform: translate(0,0);
			}
		}

		@keyframes animate-phoneicon {
			0% {
				transform: rotate(0deg);
			}

			40% {
				transform: rotate(20deg);
			}

			90% {
				transform: rotate(0deg);
			}
		}
		header div h2 {
			animation:animate-h2 5s infinite;
		}
		header div h3 {
			animation: animate-phonenumber 0.5s infinite;
		}
		#phoneicon {
			animation: animate-phoneicon 1s infinite;
		}

		@media (max-width: 767px) {
			#nav2 {
				display:block;
				background:#dbdee0;
			}
			#nav1 {
				display:none;
			}
			#nav2 .nav-link {
				color:white;
			}
			#nav2 .nav-link:hover {
				color:#5d6675;
			}
		}
    </style>

</head>

<body>
    <header>
            <div class="bg-info text-white">
                <h2>Travel <img src="images/logo.png" width="10%" height="55%"> Experts</h2>
                <h3><i id="phoneicon" class="fas fa-phone"></i> Call Us: 1-403-271-9872</h3>
            </div>

            <nav id="nav1">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="packages.php">Packages</a></li>
                    <li><a href="customer_registration_form.php">Register</a></li>
					<li><a href="login.php">Log in</a></li>
                    <li><a href="AgencyContact.php">Contact</a></li>
                </ul>
            </nav>

<!-------------------------------------------------------------------------------------------------->
			<!--Navbar-->
			<nav id="nav2" class="navbar navbar-light light-blue lighten-4">

			<!-- Navbar brand -->
			<a class="navbar-brand" href="#"></a>

			<!-- Collapse button -->
			<button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
				aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fa fa-bars fa-1x"></i></span></button>

			<!-- Collapsible content -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent1">

				<!-- Links -->
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="packages.php">Vacation Package</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="customer_registration_form.php">Register</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="agent_login.php">Log in</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="AgencyContact.php">Contact</a>
					</li>
				</ul>
				<!-- Links -->

			</div>
			<!-- Collapsible content -->

			</nav>
			<!--/.Navbar-->

    </header>
