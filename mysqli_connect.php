
<!-- Coded By:  Carol Bing He -->

<?php

//Credentials
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";

//Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//Check connection
if (!$conn) {
    trigger_error(mysqli_connect_error());
} 

?>