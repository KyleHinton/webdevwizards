// Created By : Marc Javier
//Event Listeners for Order Package

// DELETE THIS FILE

document.getElementById("postal_code_address").addEventListener("focusout", checkPostal());

function checkPostal() {
    var value = document.getElementById("postal_code_address");
    var regex = /[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]\d/;
    if (!regex.exec(value.value)) {
      alert("Not a valid Postal Code");
      value.blur();
      return false;
    };
  };


document.getElementsByClassName