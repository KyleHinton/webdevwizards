<!-- Created By: Marc Javier -->
<!-- DELETE THIS FILE -->
<?php
// include ('customer_class.php');

class Order
{
    // Personal Information
    private $custFirstName;
    private $custMidInitial;
    private $custLastName;
    private $custDateOfBirth;
    // Contact Information
    private $custAddress;
    private $custCity;
    private $custProvince;
    private $custPostalCode;
    private $custCountry;
    private $custHomePhone;
    private $custBusPhone;
    private $custEmail;
    // Payment Information
    private $custCardNumber;
    private $custCardExpiry;
    private $custCardSecurity;
    private $custCardName;

    // Setters & Getters
    public function setCustFirstName($custFirstName) {
        $this -> custFirstName = $custFirstName;
    }

    public function getCustFirstName() {
        return $this -> custFirstName;
    }

    public function setCustMidInitial($custMidInitial) {
        $this -> custMidInitial = $custMidInitial;
    }

    public function getCustMidInitial() {
        return $this -> custMidInitial;
    }

    public function setCustLastName($custLastName) {
        $this -> custLastName = $custLastName;
    }

    public function getCustLastName() {
        return $this -> custLastName;
    }

    public function setCustDateOfBirth($custDateOfBirth) {
        $this -> custDateOfBirth = $custDateOfBirth;
    }

    public function getCustDateOfBirth() {
        return $this -> custDateOfBirth;
    }

    public function setCustAddress($custAddress) {
        $this -> custAddress = $custAddress;
    }

    public function getCustAddress() {
        return $this -> custAddress;
    }

    public function setCustCity($custCity) {
        return $this -> custCity = $custCity;
    }

    public function getCustCity() {
        return $this -> custCity;
    }

    public function setCustProvince($custProvince) {
        return $this -> custProvince = $custProvince;
    }

    public function getCustProvince() {
        return $this -> custProvince;
    }

    public function setCustPostalCode($custPostalCode) {
        return $this -> custPostalCode = $custPostalCode;
    }

    public function getCustPostalCode() {
        return $this -> custPostalCode;
    }

    public function setCustCountry($custCountry) {
        return $this -> custCountry = $custCountry;
    }

    public function getCustCountry() {
        return $this -> custCountry;
    }

    public function setCustHomePhone($custHomePhone) {
        return $this -> custHomePhone = $custHomePhone;
    }

    public function getCustHomePhone() {
        return $this -> custHomePhone;
    }

    public function setCustBusPhone($custBusPhone) {
        return $this -> custBusPhone = $custBusPhone;
    }

    public function getCustBusPhone() {
        return $this -> custBusPhone;
    }

    public function setCustEmail($custEmail) {
        return $this -> custEmail = $custEmail;
    }

    public function getCustEmail() {
        return $this -> custEmail;
    }

    public function setCustCardNumber($custCardNumber) {
        return $this -> custCardNumber = $custCardNumber;
    }

    public function getCustCardNumber() {
        return $this -> custCardNumber;
    }

    public function setCustCardExpiry($custCardExpiry) {
        return $this -> custCardExpiry = $custCardExpiry;
    }

    public function getCustCardExpiry() {
        return $this -> custCardExpiry;
    }

    public function setCustCardSecurity($custCardSecurity) {
        return $this -> custCardSecurity = $custCardSecurity;
    }

    public function getCustCardSecurity() {
        return $this -> custCardSecurity;
    }

    public function setCustCardName($custCardName) {
        return $this -> custCardName = $custCardName;
    }

    public function getCustCardName() {
        return $this -> custCardName;
    }

    public function __construct($input) {
        $this -> $custFirstName = $input['first-name'];
        $this -> $custMidInitial = $input['mid-initial'];
        $this -> $custLastName = $input['last-name'];
        $this -> $custDateOfBirth = $input['date-of-birth'];
        $this -> $custAddress = $input['street-address'];
        $this -> $custCity = $input['city-address'];
        $this -> $custProvince = $input['province-address'];
        $this -> $custPostalCode = $input['postal-code-address'];
        $this -> $custCountry = $input['country-address'];
        $this -> $custHomePhone = $input['contact-home-phone'];
        $this -> $custBusPhone = $input['contact-bus-phone'];
        $this -> $custEmail = $input['contact-email'];
        $this -> $custCardNumber = $input['order-card-number'];
        $this -> $custCardExpiry = $input['order-card-expiry'];
        $this -> $custCardSecurity = $input['order-card-security'];
        $this -> $custCardName = $input['order-card-name'];
    }

    public function __toString() {
        echo "Personal Information<br />";
        echo "First Name: " . $this -> getCustFirstName() . "<br />";
        echo "Middle Initial: " . $this -> getCustMidInitial() . "<br />";
        echo "Last Name: " . $this -> getCustLastName() . "<br />";
        echo "DOB: " . $this -> getCustDateOfBirth() . "<br />";
        echo "ContactInformation<br />";
        echo "Street Address: " . $this -> getCustAddress() . "<br />";
        echo "City: " . $this -> getCustCity() . "<br />";
        echo "Province: " . $this -> getCustProvince() . "<br />";
        echo "Postal Code: " . $this -> getCustPostalCode() . "<br />";
        echo "Country: " . $this -> getCustCountry() . "<br />";
        echo "Home Phone: " . $this -> getCustHomePhone() . "<br />";
        echo "Business Phone: " . $this -> getCustBusPhone() . "<br />";
        echo "Email: " . $this -> getCustEmail() . "<br />";
        echo "Payment Information<br />";
        echo "Credit Card Number: " . $this -> getCustCardNumber() . "<br />";
        echo "Credit Card Expiry: " . $this -> getCustCardExpiry() . "<br />";
        echo "Credit Card Security: " . $this -> getCustCardSecurity() . "<br />";
        echo "Credit Card Name: " . $this -> getCustCardName() . "<br />";
    }
}