


<?php
/*----------------------------------------------*/
/*Author: Kyle Hinton							*/
/*Student ID: 000776182							*/
/*E-mail: kyle.hinton@edu.sait.ca				*/
/*-----------------------------------------------*/

//This function is for putting data into the database.
//The $data is the value of the data, the $queryString contains the keys of the column's for the form.
//The $table is the name of the table in the database.

function createData($data, $table,$queryString){

//Variable declaration
$host = "localhost";
$username = "root";
$password = "";
$dbname = "travelexperts";

//Object oriented approach to making a connection. Made an object as a connection. 
//Takes the host, username, password and database name as parameters
$mysqli = new mysqli($host,  $username, $password, $dbname);{
	if($mysqli-> connect_error){
		die("Error number: " . $mysqli->connect_error); //exit if there is a connection error. print the error message.
	}
	else {echo "Connection Successful!";}
}

//declare and create an associative array from the array of data.
$returnString = "'".implode("','", $data). "'";

//Insert the information to the database using the queryString parameters. Get the new agent data and apply the array in returnString to INSERT
//querystring is the key and the returnstring has the values
$sql ="INSERT into $table " . $queryString ."values($returnString) ";
if($mysqli->query($sql){
	echo "A new agent was added successfully!";
} else{
	echo "Error: " . $sql . "<br>" . $mysqli->error;
}
$mysqli->close();


}

?>
